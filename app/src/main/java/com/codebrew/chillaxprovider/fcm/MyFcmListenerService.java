package com.codebrew.chillaxprovider.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.activities.AcceptServiceActivity;
import com.codebrew.chillaxprovider.activities.HomeActivity;
import com.codebrew.chillaxprovider.activities.StartServiceActivity;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MyFcmListenerService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage message) {
        sendNotification(message);
    }


    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(RemoteMessage messageBody) {
        try {
            Intent intent;
            String status = messageBody.getData().get("status");
            switch (status.toLowerCase()) {
                case "new_order":
                    intent = new Intent(this, AcceptServiceActivity.class)
                            .putExtra(DataNames.ORDER_ID, messageBody.getData().get("orderId"));
                    break;
                case "accept":
                case "arrived":
                case "start":
                    Intent intentBr = new Intent();
                    intentBr.setAction(DataNames.INTENT_REFRESH_FROM_NOTIFICATION);
                    intentBr.putExtra(DataNames.ORDER_ID, messageBody.getData().get("orderId"));
                    sendBroadcast(intentBr);
                    intent = new Intent(this, StartServiceActivity.class)
                            .putExtra(DataNames.ORDER_ID, messageBody.getData().get("orderId"));
                    break;
                default:
                    intent = new Intent(this, HomeActivity.class);
                    break;
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.ic_logo)
                    .setContentTitle(getString(R.string.app_name))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody.getData().get("message")))
                    .setAutoCancel(true)
                    .setSound(sound)
                    .setContentText(messageBody.getData().get("message"))
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (messageBody.getData().containsKey("orderId"))
                notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
            else
                notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}