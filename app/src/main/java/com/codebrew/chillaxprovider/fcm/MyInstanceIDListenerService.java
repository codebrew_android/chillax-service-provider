package com.codebrew.chillaxprovider.fcm;

import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyInstanceIDListenerService extends FirebaseInstanceIdService {

    private static final String TAG = "TOKEN";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        // TODO: Implement this method to send any registration to your app's servers.
//      sendRegistrationToServer(refreshedToken);
        /*if (StaticFunction.getAccessToken(this) != null
                && !StaticFunction.getAccessToken(this).equals(""))
            updateRegid(refreshedToken);*/
        Prefs.with(this).save(DataNames.PUSH_TOKEN, refreshedToken);
    }

   /* private void updateRegid(String token) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("access_token", StaticFunction.getAccessToken(this));
        hashMap.put("reg_id", token);

        Call<PojoSuccess> successCall = RestClient.getModalApiService().setRegId(hashMap);

        successCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
            }
        });
    }*/


}