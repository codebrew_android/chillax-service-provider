package com.codebrew.chillaxprovider.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.Category;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
 * Created by cbl80 on 30/11/16.
 */
public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private Context mContext;
    private List<Category> data = new ArrayList<>();
    public int width = 0;
    public int height = 0;

    public ServiceAdapter(Context context, List<Category> list) {
        mContext = context;
        data.clear();
        data.addAll(list);
        width = GeneralFunctions.getScreenWidth(mContext);
        height = (int) (width / 3.2);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_service, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.itemView.getLayoutParams().height = height;
        Category category = data.get(position);
      //  holder.tvService.setText(category.categoryId.multiDetails.get(0).name);
        if (category.isApproved)
        {
            holder.isActive.setVisibility(View.VISIBLE);
            holder.tvService.setTextColor(ContextCompat.getColor(mContext,R.color.dark_theme_color));
        }else
        {
            holder.isActive.setVisibility(View.GONE);
            holder.tvService.setTextColor(ContextCompat.getColor(mContext,R.color.pinkish_grey));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvService2)
        TextView tvService;

        @Bind(R.id.isActive)
        ImageView isActive;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvService.setTypeface(AppGlobal.avn_roman);
        }
    }

}