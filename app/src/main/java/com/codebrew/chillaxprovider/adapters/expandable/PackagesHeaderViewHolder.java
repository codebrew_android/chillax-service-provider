package com.codebrew.chillaxprovider.adapters.expandable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.DatumCatList;
import com.codebrew.chillaxprovider.models.ListPackagesSupplier;
import com.codebrew.chillaxprovider.utils.AppGlobal;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PackagesHeaderViewHolder extends ParentViewHolder implements View.OnClickListener {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    @Bind(R.id.tvUserName)
    TextView tvUserName;


    public PackagesHeaderViewHolder(View itemView, final Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        tvUserName.setTypeface(AppGlobal.avn_heavy);
    }

    public void bind(DatumCatList packages) {
        tvUserName.setText(packages.multiDetails.get(0).name);

    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
                //rotateImage.setRotation(ROTATED_POSITION);
            } else {
                //rotateImage.setRotation(INITIAL_POSITION);
            }
        }
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
        }
    }
}
