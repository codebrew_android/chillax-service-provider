package com.codebrew.chillaxprovider.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.activities.AcceptServiceActivity;
import com.codebrew.chillaxprovider.activities.StartServiceActivity;
import com.codebrew.chillaxprovider.models.DatumPendingOrder;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.facebook.drawee.view.SimpleDraweeView;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
 * Created by cbl80 on 24/11/16.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private Context mContext;
    private List<DatumPendingOrder> alData = new ArrayList<>();
    public int width = 0;
    public int height = 0;

    public HistoryAdapter(Context context, List<DatumPendingOrder> list) {
        mContext = context;
        alData.clear();
        alData.addAll(list);
        width = GeneralFunctions.getScreenWidth(mContext);
        height = (int) (width * 0.7);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        DatumPendingOrder data = alData.get(position);
        if (data.userId.profilePicURL != null && data.userId.profilePicURL.original != null)
            holder.sdvProfile.setImageURI(data.userId.profilePicURL.original);
        holder.tvUserName.setText(data.userId.name);
        holder.tvStatus.setText(data.status);
        holder.tvService.setText(MessageFormat.format("{0}{1}", mContext.getString(R.string.service), data.details.get(0).productId.multiDetails.get(0).name));
        holder.tvPrice.setText(MessageFormat.format("{0}{1}", "$", String.valueOf(data.netAmount)));
        holder.tvTime.setText(GeneralFunctions.getComparePager(data.date));
    }

    @Override
    public int getItemCount() {
        return alData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.sdvProfile)
        SimpleDraweeView sdvProfile;

        @Bind(R.id.tvUserName)
        TextView tvUserName;

        @Bind(R.id.tvStatus)
        TextView tvStatus;

        @Bind(R.id.tvTime)
        TextView tvTime;

        @Bind(R.id.tvPrice)
        TextView tvPrice;

        @Bind(R.id.tvService)
        TextView tvService;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvUserName.setTypeface(AppGlobal.avn_book);
            tvStatus.setTypeface(AppGlobal.avn_book);
            tvTime.setTypeface(AppGlobal.avn_book);
            tvPrice.setTypeface(AppGlobal.avn_heavy);
            tvService.setTypeface(AppGlobal.avn_heavy);
            tvStatus.setTypeface(AppGlobal.avn_heavy);

            sdvProfile.getHierarchy().setPlaceholderImage(ContextCompat.getDrawable(mContext, R.drawable.com_facebook_profile_picture_blank_portrait));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!alData.get(getAdapterPosition()).status.toLowerCase().equals("end")) {
                        if (alData.get(getAdapterPosition()).status.toLowerCase().equals("pending")) {
                            Intent intent = new Intent(mContext, AcceptServiceActivity.class);
                            intent.putExtra(DataNames.ORDER_ID, alData.get(getAdapterPosition())
                                    ._id);
                            mContext.startActivity(intent);
                            ((Activity) mContext).overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                        } else {
                            Intent intent = new Intent(mContext, StartServiceActivity.class);
                            intent.putExtra(DataNames.ORDER_ID, alData.get(getAdapterPosition())
                                    ._id);
                            mContext.startActivity(intent);
                            ((Activity) mContext).overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                        }
                    }
                }
            });
        }
    }
}