package com.codebrew.chillaxprovider.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.DatumCity;
import com.codebrew.chillaxprovider.models.DatumPendingOrder;
import com.codebrew.chillaxprovider.utils.AppGlobal;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
 * Created by cbl80 on 26/12/16.
 */
public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private Context mContext;
    private List<DatumCity> alData = new ArrayList<>();

    public CityAdapter(Context context, List<DatumCity> list) {
        mContext = context;
        alData.clear();
        alData.addAll(list);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_tv_for_alert_dialog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
      holder.tvText.setText(alData.get(position).multiDetails.get(0).name);
    }

    @Override
    public int getItemCount() {
        return alData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.tvText)
        TextView tvText;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvText.setTypeface(AppGlobal.avn_book);

        }
    }
}