package com.codebrew.chillaxprovider.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.expandable.DetCatHeaderViewHolder;
import com.codebrew.chillaxprovider.adapters.expandable.PackagesFooterViewHolder;
import com.codebrew.chillaxprovider.models.ProductList;
import com.codebrew.chillaxprovider.models.SubCategoryList;

import java.util.List;

/*
 * Created by cbl80 on 30/11/16.
 */
public class DetSubCatExpandAdapter extends ExpandableRecyclerAdapter<DetCatHeaderViewHolder, PackagesFooterViewHolder> {

    private LayoutInflater mInflator;
    private Context context;

    public DetSubCatExpandAdapter(Context context, List<SubCategoryList> parentItemList) {
        super(parentItemList);
        mInflator = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public DetCatHeaderViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View recipeView = mInflator.inflate(R.layout.item_subscripe, parentViewGroup, false);
        return new DetCatHeaderViewHolder(recipeView, context);
    }

    @Override
    public PackagesFooterViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View ingredientView = mInflator.inflate(R.layout.all_cat_item_footer, childViewGroup, false);
        return new PackagesFooterViewHolder(ingredientView, context);
    }

    @Override
    public void onBindParentViewHolder(final DetCatHeaderViewHolder recipeViewHolder, int position, ParentListItem parentListItem) {
        SubCategoryList recipe = (SubCategoryList) parentListItem;
        recipeViewHolder.bind(recipe);
    }

    @Override
    public void onBindChildViewHolder(final PackagesFooterViewHolder ingredientViewHolder, final int position, Object childListItem) {
        final ProductList categoryData = (ProductList) childListItem;
        ingredientViewHolder.bind(categoryData);

    }

}