package com.codebrew.chillaxprovider.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.facebook.common.util.UriUtil;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
 * Created by cbl80 on 24/11/16.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private Context mContext;
    private List<String> data = new ArrayList<>();
    public int width = 0;
    public int height = 0;

    public NotificationAdapter(Context context, List<String> list) {
        mContext = context;
        data.clear();
        data.addAll(list);
        width = GeneralFunctions.getScreenWidth(mContext);
        height = (int) (width * 0.7);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.sdvProfile)
        SimpleDraweeView sdvProfile;

        @Bind(R.id.tvCat)
        TextView tvCat;

        @Bind(R.id.tvSubCat)
        TextView tvSubCat;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            tvCat.setTypeface(AppGlobal.avn_book);
            tvSubCat.setTypeface(AppGlobal.avn_book);
            Uri uri = new Uri.Builder()
                    .scheme(UriUtil.LOCAL_RESOURCE_SCHEME) // "res"
                    .path(String.valueOf(R.drawable.com_facebook_profile_picture_blank_portrait))
                    .build();
            sdvProfile.setImageURI(uri);
        }
    }

}