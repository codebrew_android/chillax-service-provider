package com.codebrew.chillaxprovider.adapters.expandable;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.DetSubCatExpandAdapter;
import com.codebrew.chillaxprovider.models.SubCategories;
import com.codebrew.chillaxprovider.models.SubCategoryList;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
 * Created by cbl80 on 30/11/16.
 */
public class SubFooterViewHolder extends ChildViewHolder {
    @Bind(R.id.recylerView)
    RecyclerView recylerView;

    private Context mContext;

    public SubFooterViewHolder(View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mContext=context;
        recylerView.setLayoutManager(new LinearLayoutManager(mContext));

    }

    public void bind(SubCategoryList ingredient) {
        List<SubCategoryList> subCategoryLists=new ArrayList<>();
        subCategoryLists.add(ingredient);
        recylerView.setAdapter(new DetSubCatExpandAdapter(mContext, subCategoryLists));
    }
}