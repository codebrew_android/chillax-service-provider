package com.codebrew.chillaxprovider.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;


import com.codebrew.chillaxprovider.fragments.FragCategories;
import com.codebrew.chillaxprovider.fragments.FragPersonalInfo;

import java.util.List;

public class PagerAdater extends FragmentPagerAdapter {
    private final List<String> tabTitles;

    public PagerAdater(FragmentManager fragmentManager, List<String> tabTitles) {
        super(fragmentManager);
        this.tabTitles = tabTitles;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return tabTitles.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return FragCategories.newInstance(position);
        } else {
            return FragPersonalInfo.newInstance(position);
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }

}