package com.codebrew.chillaxprovider.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.expandable.PackagesHeaderViewHolder;
import com.codebrew.chillaxprovider.adapters.expandable.SubFooterViewHolder;
import com.codebrew.chillaxprovider.models.DatumCatList;
import com.codebrew.chillaxprovider.models.SubCategories;
import com.codebrew.chillaxprovider.models.SubCategoryList;

import java.util.List;

/*
 * Created by cbl80 on 7/5/16.
 */
public class CatExpandableAdapter extends ExpandableRecyclerAdapter<PackagesHeaderViewHolder, SubFooterViewHolder> {

    private List<DatumCatList> dataSupplier;
    private LayoutInflater mInflator;
    private Context context;

    public CatExpandableAdapter(Context context, List<DatumCatList> parentItemList) {
        super(parentItemList);
        mInflator = LayoutInflater.from(context);
        this.context = context;
        dataSupplier = parentItemList;
    }

    @Override
    public PackagesHeaderViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View recipeView = mInflator.inflate(R.layout.all_cat_item_header, parentViewGroup, false);
        return new PackagesHeaderViewHolder(recipeView, context);
    }

    @Override
    public SubFooterViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View ingredientView = mInflator.inflate(R.layout.item_recylerview, childViewGroup, false);
        return new SubFooterViewHolder(ingredientView, context);
    }

    @Override
    public void onBindParentViewHolder(final PackagesHeaderViewHolder recipeViewHolder, int position, ParentListItem parentListItem) {
        DatumCatList recipe = (DatumCatList) parentListItem;
        recipeViewHolder.bind(recipe);
    }

    @Override
    public void onBindChildViewHolder(final SubFooterViewHolder ingredientViewHolder, final int position, Object childListItem) {
        final SubCategoryList categoryData = (SubCategoryList) childListItem;
        ingredientViewHolder.bind(categoryData);

    }

}
