package com.codebrew.chillaxprovider.adapters.expandable;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.models.ProductList;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.dialogs.GeneralDialog;
import com.codebrew.chillaxprovider.utils.dialogs.PriceDialog;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PackagesFooterViewHolder extends ChildViewHolder {
    @Bind(R.id.cbSelector)
    CheckBox cbSelector;

    @Bind(R.id.tvCount)
    TextView tvCount;
    private Context mContext;

    @Bind(R.id.tvProductName)
    TextView tvProductName;

    private ProductList productList;

    public PackagesFooterViewHolder(View itemView, final Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mContext = context;
        tvProductName.setTypeface(AppGlobal.avn_book);
        tvCount.setTypeface(AppGlobal.avn_medium);
        cbSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    PriceDialog peopleDialog = new PriceDialog(mContext, new PriceDialog.OnOkClickListener() {
                        @Override
                        public void onButtonClick(String count) {
                            tvCount.setText(MessageFormat.format("$ {0}", count));
                            priceApi(context, count);
                        }
                    }, 0);
                    peopleDialog.show();
                }
            }
        });
    }

    private void priceApi(final Context context, String count) {
        ProgressBarDialog.showProgressBar(context);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("languageId", StaticFunction.getLanguageId(context));
        hashMap.put("price", count);
        hashMap.put("productId", productList._id);
        hashMap.put("subCategoryId", productList.subcategoryId);
        hashMap.put("categoryId", productList.categoryId);
        Call<PojoSuccess> successCall = RestClient.getModalApiService().enterPrice(StaticFunction.getAccessToken(context)
                , hashMap);
        successCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    showDialog();
                    Intent intent = new Intent();
                    intent.setAction("refresh");
                    context.sendBroadcast(intent);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), context, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDialog() {
        GeneralDialog generalDialog = new GeneralDialog(mContext, new GeneralDialog.OnOkClickListener() {
            @Override
            public void onButtonClick() {

            }
        }, mContext.getString(R.string.tnhks_subcription), mContext.getString(R.string.msg_subcriptoiopn));
        generalDialog.show();
    }
    public void bind(ProductList ingredient) {
        productList = ingredient;
        tvProductName.setText(ingredient.multiDetails.get(0).name);
    }
}