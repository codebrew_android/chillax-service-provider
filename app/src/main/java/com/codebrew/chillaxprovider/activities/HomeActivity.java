package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.PagerAdater;
import com.codebrew.chillaxprovider.models.Data;
import com.codebrew.chillaxprovider.models.DataProfile;
import com.codebrew.chillaxprovider.models.PojoProfile;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.pagetabstrip.PagerSlidingTabStrip;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends LocationBaseActivity {


    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.sdvProfile)
    SimpleDraweeView sdvProfile;

    @Bind(R.id.sdvProfile1)
    SimpleDraweeView sdvProfile1;

    @Bind(R.id.tvUserName)
    TextView tvUserName;

    @Bind(R.id.tvSettings)
    TextView tvSettings;

    @Bind(R.id.tvPaymentMethod)
    TextView tvPaymentMethod;

    @Bind(R.id.tvAddress)
    TextView tvAddress;

    @Bind(R.id.tvHistory)
    TextView tvHistory;

    @Bind(R.id.tvLogout)
    TextView tvLogout;

    @Bind(R.id.tvUserName1)
    TextView tvUserName1;

    @Bind(R.id.tvRev)
    TextView tvRev;

    @Bind(R.id.tvRevValue)
    TextView tvRevValue;

    @Bind(R.id.tvRating)
    TextView tvRating;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.psCategories)
    PagerSlidingTabStrip psCategories;

    @Bind(R.id.vpPager)
    ViewPager viewPager;

    @Bind(R.id.ratingBar)
    RatingBar ratingBar;

    @Bind(R.id.cbSelector)
    CheckBox cbSelector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        settoolbar();
        setTypeface();
        initialise();
        changeStatusBarColor();
    }

    private void updateLatLng() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("lat", String.valueOf(latLongCurrentLocation.latitude));
        hashMap.put("long", String.valueOf(latLongCurrentLocation.longitude));
        Call<PojoSuccess> call = RestClient.getModalApiService().apiUpdateLocation(StaticFunction.getAccessToken(HomeActivity.this), hashMap);
        call.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (!response.isSuccessful()) {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), HomeActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(HomeActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white_three));
        }
    }

    private void setTypeface() {
        tvTitle.setTypeface(AppGlobal.avn_medium);
        tvUserName.setTypeface(AppGlobal.avn_medium);
        tvSettings.setTypeface(AppGlobal.avn_roman);
        tvPaymentMethod.setTypeface(AppGlobal.avn_roman);
        tvAddress.setTypeface(AppGlobal.avn_roman);
        tvHistory.setTypeface(AppGlobal.avn_roman);
        tvLogout.setTypeface(AppGlobal.avn_roman);
        tvUserName1.setTypeface(AppGlobal.avn_medium);
        tvRev.setTypeface(AppGlobal.avn_medium);
        tvRevValue.setTypeface(AppGlobal.avn_medium);
        tvRating.setTypeface(AppGlobal.avn_medium);
        tvStatus.setTypeface(AppGlobal.avn_medium);
    }

    private void initialise() {
        sdvProfile1.getHierarchy().setPlaceholderImage(ContextCompat.getDrawable(this, R.drawable.com_facebook_profile_picture_blank_portrait));
        Data pojoLogin = Prefs.with(this).getObject(DataNames.USER_DATA, Data.class);
        tvUserName.setText(pojoLogin.name);
        tvUserName1.setText(pojoLogin.name);
        sdvProfile.getHierarchy().setPlaceholderImage(ContextCompat.getDrawable(this, R.drawable.com_facebook_profile_picture_blank_portrait));
        if (pojoLogin.profilePicURL.original != null) {
            sdvProfile.setImageURI(Uri.parse(pojoLogin.profilePicURL.original));
            sdvProfile1.setImageURI(Uri.parse(pojoLogin.profilePicURL.original));
        }
        cbSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stausApi(isChecked);
            }
        });

        getProfile();
    }

    private void stausApi(boolean isChecked) {
        Call<PojoSuccess> statusCall = RestClient.getModalApiService().statusUpdate(StaticFunction.getAccessToken(this), isChecked);
        statusCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(HomeActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), HomeActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                Toast.makeText(HomeActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getProfile() {
        Call<PojoProfile> profileCall = RestClient.getModalApiService().getProfile(StaticFunction.getAccessToken(this));
        profileCall.enqueue(new Callback<PojoProfile>() {
            @Override
            public void onResponse(Call<PojoProfile> call, Response<PojoProfile> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    setData(response.body().data);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), HomeActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoProfile> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(HomeActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData(DataProfile dataProfile) {
        Prefs.with(this).save(DataNames.USER_PROFILE, dataProfile);
        List<String> strings = new ArrayList<>();
        strings.add(getString(R.string.my_serv));
        strings.add(getString(R.string.pers_info));
        PagerAdater searchAdapter = new PagerAdater(getSupportFragmentManager(), strings);
        viewPager.setAdapter(searchAdapter);
        viewPager.setOffscreenPageLimit(2);
        psCategories.setIndicatorColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        psCategories.setTextSize((int) getResources().getDimension(R.dimen.text_size_14));
        psCategories.setAllCaps(true);
        psCategories.setTypeface(AppGlobal.avn_medium, 0);
        psCategories.setDeactivateTextColor(ContextCompat.getColor(this, R.color.black_20));
        psCategories.setActivateTextColor(ContextCompat.getColor(this, R.color.pager_text_color));
        psCategories.setViewPager(viewPager);
        tvUserName.setText(dataProfile.name);
        tvUserName1.setText(dataProfile.name);
        sdvProfile.getHierarchy().setPlaceholderImage(ContextCompat.getDrawable(this, R.drawable.com_facebook_profile_picture_blank_portrait));
        if (dataProfile.profilePicURL.original != null) {
            sdvProfile.setImageURI(Uri.parse(dataProfile.profilePicURL.original));
            sdvProfile1.setImageURI(Uri.parse(dataProfile.profilePicURL.original));
        }
        tvRevValue.setText(String.valueOf(dataProfile.reviews));
        if (dataProfile.ratings != null)
            ratingBar.setRating(dataProfile.ratings);
        cbSelector.setChecked(dataProfile.status);

    }


    private void settoolbar() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawrbtn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    public void openDrawar() {
        if (!drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.openDrawer(GravityCompat.START);
        }
    }

    public void closeDrawar() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            closeDrawar();
        } else {
            finish();
        }


    }


    @OnClick({R.id.tvHistory, R.id.tvPaymentMethod, R.id.tvAddress, R.id.tvLogout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvHistory:
                startActivity(HistoryActivity.class);
                break;
            case R.id.tvPaymentMethod:
                startActivity(EarningsActivity.class);
                break;
            case R.id.tvAddress:
                startActivity(ManageServicesActivity.class);
                break;
            case R.id.tvLogout:
                StaticFunction.showLogoutAlert(HomeActivity.this);
                break;
        }
        closeDrawar();
    }

    private void startActivity(Class class_) {
        startActivity(new Intent(HomeActivity.this, class_));
        overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                openDrawar();
                break;
            case R.id.notification:
                startActivity(NotificationActivity.class);
                break;
        }

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        latLongCurrentLocation=new LatLng(location.getLatitude(),location.getLongitude());
        updateLatLng();
    }
}
