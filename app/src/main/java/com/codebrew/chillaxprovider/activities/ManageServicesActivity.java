package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.DetSubCaty;
import com.codebrew.chillaxprovider.models.DetailedSubCategory;
import com.codebrew.chillaxprovider.models.ListPackagesSupplier;
import com.codebrew.chillaxprovider.models.SubCategories;
import com.codebrew.chillaxprovider.utils.AppGlobal;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
 * Created by cbl80 on 1/12/16.
 */
public class ManageServicesActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.rvCategories)
    RecyclerView rvCategories;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        settoolbar();
        setTypeface();
        initialise();
        changeStatusBarColor();
    }


    private void initialise() {
        tvTitle.setText(getString(R.string.manage_services));
        rvCategories.setNestedScrollingEnabled(false);
        rvCategories.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setTypeface() {
        tvTitle.setTypeface(AppGlobal.avn_medium);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        }
    }


    private void settoolbar() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_colour);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in_fast, R.anim.slide_right_out_fast);
    }
}


