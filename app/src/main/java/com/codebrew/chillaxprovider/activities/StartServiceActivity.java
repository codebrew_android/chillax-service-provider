package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.DataOrderDetais;
import com.codebrew.chillaxprovider.models.PojoOrderDetails;
import com.codebrew.chillaxprovider.models.PojoPendingOrder;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.models.pojodirection.PojoDirection;
import com.codebrew.chillaxprovider.models.pojodirection.RouteList;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.dialogs.DialogCancellation;
import com.codebrew.chillaxprovider.utils.dialogs.DialogReason;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartServiceActivity extends LocationBaseActivity {

    @Bind(R.id.mapView)
    MapView mMapView;

    @Bind(R.id.llButtons)
    LinearLayout llButtons;

    @Bind(R.id.ivDot1)
    ImageView ivDot1;

    @Bind(R.id.ivDot2)
    ImageView ivDot2;

    @Bind(R.id.ivDot3)
    ImageView ivDot3;

    @Bind(R.id.tvStatus)
    TextView tvStatus;


    @Bind(R.id.tvEnd)
    TextView tvEnd;

    @Bind(R.id.tvUserName)
    TextView tvUserName;

    @Bind(R.id.tvAddress)
    TextView tvAddress;

    @Bind(R.id.sdvProfile)
    SimpleDraweeView sdvProfile;


    private GoogleMap mGoogleMap;
    private LatLng latLongCurrentLocation, latLongDestination;
    private String orderId;
    private DataOrderDetais dataOrderDetais;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_service);
        ButterKnife.bind(this);
        ProgressBarDialog.showProgressBar(this);
        initializeMap(savedInstanceState, 0, 0);
        initData();
        changeStatusBarColor();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        }
    }

    private void initData() {
        orderId = getIntent().getStringExtra(DataNames.ORDER_ID);
        getData(orderId);
    }

    private void getData(String id) {
        ProgressBarDialog.showProgressBar(this);
        Call<PojoOrderDetails> detailsCall = RestClient.getModalApiService().orderDetails(id);
        detailsCall.enqueue(new Callback<PojoOrderDetails>() {
            @Override
            public void onResponse(Call<PojoOrderDetails> call, Response<PojoOrderDetails> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    setData(response.body().data.get(0));
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), StartServiceActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoOrderDetails> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(StartServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData(DataOrderDetais dataOrderDetais) {
        this.dataOrderDetais=dataOrderDetais;
        sdvProfile.getHierarchy().setPlaceholderImage(ContextCompat.getDrawable(StartServiceActivity.this, R.drawable.com_facebook_profile_picture_blank_portrait));
        if (dataOrderDetais.userId.profilePicURL.original != null)
            sdvProfile.setImageURI(dataOrderDetais.userId.profilePicURL.original);
        tvUserName.setText(dataOrderDetais.userId.name);
        tvAddress.setText(dataOrderDetais.userId.address.get(0).address);
        if (dataOrderDetais.status.toLowerCase().equals("arrived")) {
            onArrived();
        } else if (dataOrderDetais.status.toLowerCase().equals("start")) {
            tvEnd.setBackgroundColor(ContextCompat.getColor(this, R.color.dark_theme_color));
            onServiceStart();
        }
        callDirectionsApi();
    }


    @OnClick({R.id.ivCurrentLocation, R.id.tvArrived, R.id.tvCancel, R.id.tvEnd})
    public void onCLick(View view) {
        switch (view.getId()) {
            case R.id.ivCurrentLocation:
                moveCameraToBounds();
                break;
            case R.id.tvArrived:
                callArrivedApi();
                break;
            case R.id.tvCancel:
                showConfirmationDialog();
                break;
            case R.id.tvEnd:
                endService();
                break;
        }
    }

    private void endService() {
        ProgressBarDialog.showProgressBar(this);
        Call<PojoSuccess> successCall = RestClient.getModalApiService().endService(StaticFunction.getAccessToken(this)
                , orderId);
        successCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    finishAffinity();
                    (StartServiceActivity.this).startActivity(new Intent(StartServiceActivity.this, ReviewActivity.class)
                    .putExtra(DataNames.ORDER_ID,orderId)
                    .putExtra(DataNames.SUPPLIER_ID,dataOrderDetais.userId._id)
                    .putExtra(DataNames.PRODUCT_NAME,dataOrderDetais.details.get(0).productId.multiDetails.get(0).name));
                    (StartServiceActivity.this).overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), StartServiceActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(StartServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void showConfirmationDialog() {
        new DialogCancellation(this, new DialogCancellation.OnButtonClickListener() {
            @Override
            public void onButtonYesClick() {
                showReasonDialog();
            }
        }).show();
    }

    private void showReasonDialog() {
        new DialogReason(StartServiceActivity.this, new DialogReason.OnSubmitClickListener() {
            @Override
            public void onButtonClick(String reason) {
                callCancelServiceApi(reason);
            }
        }).show();
    }


    private void initializeMap(@Nullable Bundle savedInstanceState, double lat, double lng) {
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        initMap(lat, lng);
    }

    private void initMap(final double lat, final double lng) {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                setDestinationLocation(lat, lng);
            }
        });
    }

    private void setDestinationLocation(double lat, double lng) {
        mGoogleMap.clear();
        latLongDestination = new LatLng(lat, lng);  // Vendor's Location 30.7333, 76.7794
        mGoogleMap.addMarker(new MarkerOptions()
                .position(latLongDestination)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pinpoint)));
    }


    // Moving camera to desired latlong.
    private void moveCameraToBounds() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(latLongDestination);
        if (latLongCurrentLocation != null)
            builder.include(latLongCurrentLocation);
        LatLngBounds bounds = builder.build();
        int padding = 80; // offset from edges of the map in pixels
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
    }


    private void callDirectionsApi() {
        //ProgressBarDialog.showProgressBar(this);
        if (latLongCurrentLocation==null)
        {
            latLongCurrentLocation=new LatLng(0,0);
        }
        String origin = latLongCurrentLocation.latitude + "," + latLongCurrentLocation.longitude;
        String destination = latLongDestination.latitude + "," + latLongDestination.longitude;
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("origin", origin);
        hashMap.put("destination", destination);
        hashMap.put("mode", "driving");
        hashMap.put("alternatives", "true");
        hashMap.put("sensor", "true");
        Call<PojoDirection> categoryCall = RestClient.getModalApiService().getDirections(hashMap);
        categoryCall.enqueue(new Callback<PojoDirection>() {
            @Override
            public void onResponse(Call<PojoDirection> call, Response<PojoDirection> response) {
                //  ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().routes.size() > 0) {
                        setupPath(response.body().routes.get(calculateShortestRoute(response.body().routes)));
                    }
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), StartServiceActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoDirection> call, Throwable t) {
                // ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(StartServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void callArrivedApi() {
        ProgressBarDialog.showProgressBar(this);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("orderId", orderId);
        hashMap.put("reason", "sdasd");
        Call<PojoSuccess> call = RestClient.getModalApiService().apiArrived(StaticFunction.getAccessToken(StartServiceActivity.this), hashMap);
        call.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful())
                    onArrived();
                else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), StartServiceActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(StartServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void callCancelServiceApi(String reason) {
        ProgressBarDialog.showProgressBar(this);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("orderId", orderId);
        hashMap.put("reason", reason);
        Call<PojoPendingOrder> call = RestClient.getModalApiService().apiCancelService(StaticFunction.getAccessToken(StartServiceActivity.this), hashMap);
        call.enqueue(new Callback<PojoPendingOrder>() {
            @Override
            public void onResponse(Call<PojoPendingOrder> call, Response<PojoPendingOrder> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful())
                    onServiceCancel();
                else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), StartServiceActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoPendingOrder> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(StartServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void onServiceCancel() {
        Toast.makeText(StartServiceActivity.this, R.string.order_cancelled, Toast.LENGTH_SHORT).show();
        StartServiceActivity.this.startActivity(new Intent(StartServiceActivity.this, HomeActivity.class));
        StartServiceActivity.this.overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
    }

    private void onArrived() {
        llButtons.setVisibility(View.GONE);
        tvEnd.setVisibility(View.VISIBLE);
        tvStatus.setText(R.string.waiting_service);
        ivDot1.setImageDrawable(ContextCompat.getDrawable(StartServiceActivity.this, R.drawable.ic_progress_small));
        ivDot2.setImageDrawable(ContextCompat.getDrawable(StartServiceActivity.this, R.drawable.ic_progress_big));
        Toast.makeText(StartServiceActivity.this, R.string.wait_for_start, Toast.LENGTH_SHORT).show();
    }

    private void onServiceStart() {
        llButtons.setVisibility(View.GONE);
        tvEnd.setVisibility(View.VISIBLE);
        tvStatus.setText(R.string.waiting_service);
        ivDot1.setImageDrawable(ContextCompat.getDrawable(StartServiceActivity.this, R.drawable.ic_progress_small));
        ivDot2.setImageDrawable(ContextCompat.getDrawable(StartServiceActivity.this, R.drawable.ic_progress_small));
        ivDot3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_progress_big));
    }


    private void callUpdateLocationApi() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("lat", String.valueOf(latLongCurrentLocation.latitude));
        hashMap.put("long", String.valueOf(latLongCurrentLocation.longitude));
        Call<PojoSuccess> call = RestClient.getModalApiService().apiUpdateLocation(StaticFunction.getAccessToken(StartServiceActivity.this), hashMap);
        call.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (!response.isSuccessful()) {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), StartServiceActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(StartServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void setupPath(RouteList routeList) {
        ArrayList<LatLng> alPathLatLongs = new ArrayList<>();
        for (int i = 0; i < routeList.legs.get(0).steps.size(); i++) {
            alPathLatLongs.addAll(GeneralFunctions.decodePoly(routeList.legs.get(0).steps.get(i).polyline.points));
        }
        drawPolylinePath(alPathLatLongs);
    }

    private void drawPolylinePath(ArrayList<LatLng> alPathLatLongs) {
        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.addAll(alPathLatLongs);
        lineOptions.width(5);
        lineOptions.color(ContextCompat.getColor(this, R.color.colorAccent));
        mGoogleMap.addPolyline(lineOptions);
        moveCameraToBounds();
    }

    private int calculateShortestRoute(List<RouteList> routes) {
        int shortestDistance = routes.get(0).legs.get(0).distance.value;
        int distance;
        int shortestRoute = 0;
        for (int i = 0; i < routes.size(); i++) {
            distance = routes.get(i).legs.get(0).distance.value;
            if (shortestDistance > distance) {
                shortestDistance = distance;
                shortestRoute = i;
            }
        }
        return shortestRoute;
    }


    @Override
    public void onLocationUpdated(LatLng latLongCurrentLocation) {
        super.onLocationUpdated(latLongCurrentLocation);
        this.latLongCurrentLocation = new LatLng(latLongCurrentLocation.latitude, latLongCurrentLocation.longitude);
        setDestinationLocation(latLongCurrentLocation.latitude, latLongCurrentLocation.longitude);
        mGoogleMap.addMarker(new MarkerOptions()
                .position(latLongCurrentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)));
        callDirectionsApi();
        //   moveCameraToBounds();
        callUpdateLocationApi();
    }

    @Override
    public void onLocationUpdateFailure(LatLng latLongCurrentLocation) {     //It will return LatLong(0,0)
        super.onLocationUpdateFailure(latLongCurrentLocation);
        this.latLongCurrentLocation = new LatLng(latLongCurrentLocation.latitude, latLongCurrentLocation.longitude);
        moveCameraToBounds();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in_fast, R.anim.slide_right_out_fast);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(refreshReceiver, new IntentFilter(DataNames.INTENT_REFRESH_FROM_NOTIFICATION));
        if (mMapView != null) {
            try {
                mMapView.onResume();
            } catch (Exception | Error throwable) {
                throwable.printStackTrace();
            }

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
        unregisterReceiver(refreshReceiver);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null)
                getData(intent.getStringExtra(DataNames.ORDER_ID));
        }
    };
}

