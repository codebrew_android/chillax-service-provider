package com.codebrew.chillaxprovider.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

/*
 * Created by cbl80 on 1/12/16.
 */
public class EarningsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earnings);

        List<PointValue> values1 = new ArrayList<>();
        values1.add(new PointValue(0, 0));
        values1.add(new PointValue(1, 50));
        values1.add(new PointValue(2, 100));
        values1.add(new PointValue(3, 50));
        values1.add(new PointValue(4, 120));
        values1.add(new PointValue(5, 182));
        values1.add(new PointValue(6, 93));
        values1.add(new PointValue(7, 160));
        values1.add(new PointValue(8, 50));
        values1.add(new PointValue(9, 120));
        values1.add(new PointValue(10, 182));
        values1.add(new PointValue(11, 93));
        values1.add(new PointValue(12, 75));

        //In most cased you can call data model methods in builder-pattern-like manner.
        Line line1 = new Line(values1).setColor(Color.WHITE).setCubic(true );
        line1.setStrokeWidth(2);
        line1.setAreaTransparency(5);
        line1.setPointColor(ContextCompat.getColor(this,R.color.colorPointer));
        line1.setShape(ValueShape.CIRCLE);
        line1.setHasLabels(true);
        line1.setHasLabelsOnlyForSelected(true);
        line1.setPointRadius(5);
        line1.setHasLines(false);


        List<Line> lines = new ArrayList<>();
        lines.add(line1);

        LineChartData data = new LineChartData();
        data.setLines(lines);


        List<Float> xaxisvalues= new ArrayList<>();
        xaxisvalues.add(1f);
        xaxisvalues.add(2f);
        xaxisvalues.add(3f);
        xaxisvalues.add(4f);
        xaxisvalues.add(5f);
        xaxisvalues.add(6f);
        xaxisvalues.add(7f);
        xaxisvalues.add(8f);
        xaxisvalues.add(9f);
        xaxisvalues.add(10f);
        xaxisvalues.add(11f);
        xaxisvalues.add(12f);

        final List<String> xaxisvaluesLabels = new ArrayList<>();
        xaxisvaluesLabels.clear();
        xaxisvaluesLabels.add("Jan");
        xaxisvaluesLabels.add("Feb");
        xaxisvaluesLabels.add("Mar");
        xaxisvaluesLabels.add("Apr");
        xaxisvaluesLabels.add("May");
        xaxisvaluesLabels.add("Jun");
        xaxisvaluesLabels.add("Jul");
        xaxisvaluesLabels.add("Aug");
        xaxisvaluesLabels.add("Sep");
        xaxisvaluesLabels.add("Oct");
        xaxisvaluesLabels.add("Nov");
        xaxisvaluesLabels.add("Dec");

        data.setAxisXBottom(Axis.generateAxisFromCollection(xaxisvalues,xaxisvaluesLabels));
        data.setAxisYLeft(Axis.generateAxisFromRange(10,200,10));


        final LineChartView chart = (LineChartView) findViewById(R.id.chart);
        final TextView tvValue2 = (TextView) findViewById(R.id.tvValue2);
        final TextView tvValue1 = (TextView) findViewById(R.id.tvValue1);
        final TextView tvMonth = (TextView) findViewById(R.id.tvMonth);
        chart.setLineChartData(data);

        chart.setOnValueTouchListener(new LineChartOnValueSelectListener() {
            @Override
            public void onValueSelected(int i, int i1, PointValue pointValue) {
                //Toast.makeText(MainActivity.this, ""+chart.getLineChartData().getBaseValue(), Toast.LENGTH_SHORT).show();
                if (pointValue.getX() != 0) {
                    tvValue1.setText(MessageFormat.format("{0}$", pointValue.getY()));
                    tvValue2.setText(MessageFormat.format("{0}$", pointValue.getY()));
                    tvMonth.setText((xaxisvaluesLabels.get((int) pointValue.getX() - 1) + " 2016"));
                }
            }

            @Override
            public void onValueDeselected() {

            }
        });


        Viewport v = new Viewport(chart.getMaximumViewport());
        //v.left = 200;
        v.right = 6;
        chart.setCurrentViewport(v);
    }
}
