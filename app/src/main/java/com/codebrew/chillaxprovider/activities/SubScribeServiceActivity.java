package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.CatExpandableAdapter;
import com.codebrew.chillaxprovider.models.Data;
import com.codebrew.chillaxprovider.models.DatumCatList;
import com.codebrew.chillaxprovider.models.PojoCategoryListing;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;

import java.io.IOException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by cbl80 on 30/11/16.
 */
public class SubScribeServiceActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.recylerView)
    RecyclerView rvServices;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.tvContinue)
    TextView tvContinue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recylerview);
        ButterKnife.bind(this);
        settoolbar();
        setTypeface();
        changeStatusBarColor();
        initialize();
        getCatListing();
    }

    private void getCatListing() {
        ProgressBarDialog.showProgressBar(this);
        Call<PojoCategoryListing> listingCall = RestClient.getModalApiService().categoryListing(StaticFunction.getAccessToken(this)
                , StaticFunction.getLanguageId(this));

        listingCall.enqueue(new Callback<PojoCategoryListing>() {
            @Override
            public void onResponse(Call<PojoCategoryListing> call, Response<PojoCategoryListing> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    setData(response.body().data);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), SubScribeServiceActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoCategoryListing> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(SubScribeServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData(List<DatumCatList> data) {
        rvServices.setAdapter(new CatExpandableAdapter(this, data));
    }

    private void initialize() {
        rvServices.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setTypeface() {
        tvTitle.setTypeface(AppGlobal.avn_medium);
        tvContinue.setTypeface(AppGlobal.avn_medium);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        }
    }


    private void settoolbar() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in_fast, R.anim.slide_right_out_fast);
    }

    @OnClick({R.id.tvContinue})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvContinue:
                Data pojoLogin = Prefs.with(SubScribeServiceActivity.this).getObject(DataNames.USER_DATA, Data.class);
                pojoLogin.categorySelect = true;
                Prefs.with(SubScribeServiceActivity.this).save(DataNames.USER_DATA, pojoLogin);
                startActivity(new Intent(SubScribeServiceActivity.this, HomeActivity.class));
                overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            registerReceiver(refreshReceiver, new IntentFilter("refresh"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            tvContinue.setVisibility(View.VISIBLE);
        }
    };

}
