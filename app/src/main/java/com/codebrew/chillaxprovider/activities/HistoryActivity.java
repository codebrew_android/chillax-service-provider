package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.HistoryAdapter;
import com.codebrew.chillaxprovider.models.DatumPendingOrder;
import com.codebrew.chillaxprovider.models.PojoPendingOrder;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by cbl80 on 24/11/16.
 */
public class HistoryActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.rvCategories)
    RecyclerView rvCategories;

    @Bind(R.id.tvPlaceholder)
    TextView tvPlaceholder;

    List<DatumPendingOrder> alPendingOrder = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        settoolbar();
        setTypeface();
        initialise();
        changeStatusBarColor();
        apiGetPendingOrders();
    }

    private void initialise() {
        tvTitle.setText(getString(R.string.history));
        rvCategories.setNestedScrollingEnabled(false);
        rvCategories.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setTypeface() {
        tvTitle.setTypeface(AppGlobal.avn_medium);
        tvPlaceholder.setTypeface(AppGlobal.avn_medium);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        }
    }


    private void settoolbar() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_colour);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in_fast, R.anim.slide_right_out_fast);
    }

    private void apiGetPendingOrders() {
        ProgressBarDialog.showProgressBar(this);
        Call<PojoPendingOrder> profileCall = RestClient.getModalApiService().getPendingOrder(StaticFunction.getAccessToken(this));
        profileCall.enqueue(new Callback<PojoPendingOrder>() {
            @Override
            public void onResponse(Call<PojoPendingOrder> call, Response<PojoPendingOrder> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    setData(response);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), HistoryActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoPendingOrder> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(HistoryActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData(Response<PojoPendingOrder> response) {
        alPendingOrder = response.body().data;
        if (alPendingOrder.size() == 0) {
            tvPlaceholder.setVisibility(View.VISIBLE);
        } else {
            tvPlaceholder.setVisibility(View.GONE);
            rvCategories.setAdapter(new HistoryAdapter(HistoryActivity.this, alPendingOrder));
        }
    }


    //bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU4NDhlYzRkOTkwZmUxYjIxNDE3YzllYiIsInR5cGUiOiJEUklWRVIiLCJpYXQiOjE0ODIyMTA5MDJ9.F59pv8Y7B4D8yO4cLYy0hBLSHLCnHrXCpMurRlqE1sM

}
