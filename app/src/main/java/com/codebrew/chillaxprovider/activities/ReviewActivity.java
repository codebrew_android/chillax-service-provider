package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;

import java.io.IOException;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.ratingBar)
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        ButterKnife.bind(this);
        settoolbar();
        setTypeface();
        changeStatusBarColor();
    }

    private void setTypeface() {

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        }
    }

    private void settoolbar() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in_fast, R.anim.slide_right_out_fast);
    }


    @OnClick({R.id.tvSubmit,R.id.tvReport})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSubmit:
                rateApi();
                break;
            case R.id.tvReport:
                startActivity(new Intent(ReviewActivity.this,ReportActivity.class)
                        .putExtra(DataNames.ORDER_ID,getIntent().getStringExtra(DataNames.ORDER_ID)));
                overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                break;
        }
    }

    private void rateApi() {
        ProgressBarDialog.showProgressBar(this);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("orderId", getIntent().getStringExtra(DataNames.ORDER_ID));
        hashMap.put("userId", getIntent().getStringExtra(DataNames.SUPPLIER_ID));
        hashMap.put("productName", getIntent().getStringExtra(DataNames.PRODUCT_NAME));
        hashMap.put("rating", "" + ratingBar.getRating());

        Call<PojoSuccess> successCall = RestClient.getModalApiService().rateService(StaticFunction.getAccessToken(this)
                , hashMap);
        successCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    finishAffinity();
                    startActivity(new Intent(ReviewActivity.this, HomeActivity.class));
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), ReviewActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(ReviewActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
