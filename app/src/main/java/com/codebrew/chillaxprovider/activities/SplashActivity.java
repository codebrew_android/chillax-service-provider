package com.codebrew.chillaxprovider.activities;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.Toast;


import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.fragments.SplashFragment;
import com.codebrew.chillaxprovider.models.PojoAllLanguage;
import com.codebrew.chillaxprovider.models.PojoCity;
import com.codebrew.chillaxprovider.models.PojoSignUp;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.dialogs.OtpDialog;
import com.codebrew.chillaxprovider.utils.dialogs.PhoneNumberDialog;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by ankit Jindal on 21/11/16.
 */
public class SplashActivity extends FacebookLoginActivity {

    public static final String EXTRA_SHOW_ANIMATION = "extra_show_animation";
    public HashMap<String, Stack<Fragment>> mStacks;
    public String mCurrentTab = DataNames.TAB1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupTabs();
        changeStatusBarColor();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getLanguages();
            }
        }, 1000);

    }

    private void getLanguages() {
        Call<PojoAllLanguage> languageCall = RestClient.getModalApiService().getAllLanguage();
        languageCall.enqueue(new Callback<PojoAllLanguage>() {
            @Override
            public void onResponse(Call<PojoAllLanguage> call, Response<PojoAllLanguage> response) {
                if (response.isSuccessful()) {
                    Prefs.with(SplashActivity.this).save(DataNames.LANGUAGES,response.body());
                   getCityList();
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), SplashActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoAllLanguage> call, Throwable t) {
                Toast.makeText(SplashActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getCityList() {
        Call<PojoCity> languageCall = RestClient.getModalApiService().cityList(StaticFunction.getLanguageId(this));
        languageCall.enqueue(new Callback<PojoCity>() {
            @Override
            public void onResponse(Call<PojoCity> call, Response<PojoCity> response) {
                if (response.isSuccessful()) {
                    Prefs.with(SplashActivity.this).save(DataNames.CITY_LIST,response.body());
                    if (StaticFunction.loginProper(SplashActivity.this)) {
                        userLogined();
                    } else {
                        animateIcon();
                    }
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), SplashActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoCity> call, Throwable t) {
                Toast.makeText(SplashActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void userLogined() {
        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
        finishAffinity();
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public void loginSuccess(LoginResult result) {
        getUserProfile();
    }

    @Override
    public void OnSuccess(JSONObject object, GraphResponse response) {
        try {
            fbLoginApi(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fbLoginApi(JSONObject object) throws JSONException {
        ProgressBarDialog.showProgressBar(this);
        final String fbId = object.getString("id");
        final String name = "" + object.getString("name");
        final String email;
        if (object.has("email")) {
            email = object.getString("email");
        } else {
            email = fbId + "@facbook.com";
        }
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("appVersion", "" + GeneralFunctions.getVersion(this));
        hashMap.put("deviceToken", FirebaseInstanceId.getInstance().getToken());
        hashMap.put("deviceType", "ANDROID");
        hashMap.put("email", email);
        hashMap.put("image", "http://graph.facebook.com/" + fbId + "/picture?type=large");
        hashMap.put("fbId", fbId);
        hashMap.put("name", name);

        Call<PojoSignUp> fbLogin = RestClient.getModalApiService().loginWithFB(hashMap);

        fbLogin.enqueue(new Callback<PojoSignUp>() {
            @Override
            public void onResponse(Call<PojoSignUp> call, Response<PojoSignUp> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    Prefs.with(SplashActivity.this).save(DataNames.USER_DATA, response.body().data.data);
                    if (response.body().data.data.phoneVerifiy) {
                        if (response.body().data.data.categorySelect) {
                            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        }else
                        {
                            startActivity(new Intent(SplashActivity.this, SubScribeServiceActivity.class));
                        }
                        overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                        finishAffinity();
                    } else {
                        phoneDialog(response.body().data.data.accessToken);
                    }
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), SplashActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSignUp> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(SplashActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void phoneDialog(final String accessToken) {
        PhoneNumberDialog phoneNumberDilaog = new PhoneNumberDialog(this, new PhoneNumberDialog.OnOkClickListener() {
            @Override
            public void onButtonClick(String number, String countryCode) {
                phoneNumberUpdate(accessToken, number, countryCode);
            }
        });
        phoneNumberDilaog.show();
    }

    private void phoneNumberUpdate(String accessToken, String number, String countryCode) {
        ProgressBarDialog.showProgressBar(this);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("phoneNo", number);
        hashMap.put("countryCode", countryCode);

        Call<PojoSignUp> numberCall = RestClient.getModalApiService().phoneNumberUpdate("bearer " + accessToken, hashMap);

        numberCall.enqueue(new Callback<PojoSignUp>() {
            @Override
            public void onResponse(Call<PojoSignUp> call, Response<PojoSignUp> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    showOtpDilog(response.body().data.otp);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), SplashActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSignUp> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(SplashActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void animateIcon() {
        ImageView ivLogo = (ImageView) findViewById(R.id.ivLogo);
        if (getIntent().getBooleanExtra(EXTRA_SHOW_ANIMATION, true)) {
            assert ivLogo != null;
            ivLogo.animate().setStartDelay(700).translationYBy(-7 * GeneralFunctions.getScreenHeight(this) / 24)
                    .setDuration(350).setListener(
                    new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
            if (!isDestroyed())
                pushFragments(DataNames.TAB1, new SplashFragment(), true, true, "splash", false, true);
        } else {
            assert ivLogo != null;
            ivLogo.animate().translationYBy(-7 * GeneralFunctions.getScreenHeight(this) / 24)
                    .setDuration(0);
            pushFragments(DataNames.TAB1, new SplashFragment(), true, true, "splash", false, false);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.light_theme_color));
        }
    }

    public void setupTabs() {
        mStacks = new HashMap<>();
        mStacks.put(DataNames.TAB1, new Stack<Fragment>());
        mStacks.put(DataNames.TAB2, new Stack<Fragment>());
        mCurrentTab = DataNames.TAB1;
    }

    public void pushFragments(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd, String fragmentTag, boolean addFragment
            , boolean istop) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate) {
            if (istop) {
                ft.setCustomAnimations(R.anim.top_from_bottom_login, 0);
            } else {
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            }


        }

        if (addFragment) {
            ft.add(R.id.container, fragment, fragmentTag);
            ft.addToBackStack("");
        } else {
            ft.replace(R.id.container, fragment, fragmentTag);
        }
        ft.commit();

    }

    public void popFragments(boolean animation) {
      /*
       *    Select the second last fragment in current tab's stack..
       *    which will be shown after the fragment transaction given below
       */
        try {
            Fragment fragment;
            fragment = mStacks.get(mCurrentTab).elementAt(mStacks.get(mCurrentTab).size() - 2);

      /*pop current fragment from stack.. */
            mStacks.get(mCurrentTab).pop();

      /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            if (animation) {

                ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

            }
            ft.replace(R.id.container, fragment);


            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {


        switch (mCurrentTab) {
            case DataNames.TAB2:
                popFragments(true);
                break;
            case DataNames.TAB3:
                popFragments(true);
                break;
            default:
                if (mStacks.get(mCurrentTab).size() > 2) {
                    popFragments(true);

                } else if (mStacks.get(mCurrentTab).size() == 2) {
                    popFragments(true);
                } else {
                    finish();
                }
                break;
        }

    }

    private void showOtpDilog(Integer otp) {
        OtpDialog otpDialog = new OtpDialog(this, new OtpDialog.OnOkClickListener() {
            @Override
            public void onButtonClick(String otp) {
                verifyOtp(otp);
            }
        }, otp);
        otpDialog.show();
    }

    private void verifyOtp(String otp) {
        ProgressBarDialog.showProgressBar(this);
        Call<PojoSuccess> otpCall = RestClient.getModalApiService().verifyOtp(StaticFunction.getAccessToken(this)
                , otp);

        otpCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    startActivity(new Intent(SplashActivity.this, SubScribeServiceActivity.class));
                    overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                    finishAffinity();
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), SplashActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(SplashActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
