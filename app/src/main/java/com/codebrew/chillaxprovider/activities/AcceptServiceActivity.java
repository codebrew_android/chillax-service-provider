package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.DataOrderDetais;
import com.codebrew.chillaxprovider.models.PojoOrderDetails;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.models.pojodirection.PojoDirection;
import com.codebrew.chillaxprovider.models.pojodirection.RouteList;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.dialogs.DialogReason;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcceptServiceActivity extends LocationBaseActivity {

    @Bind(R.id.sdvMapImage)
    SimpleDraweeView sdvMapImage;

    @Bind(R.id.sdvUserImage)
    SimpleDraweeView sdvUserImage;

    @Bind(R.id.tvUserName)
    TextView tvUserName;

    @Bind(R.id.tvServiceName)
    TextView tvServiceName;

    @Bind(R.id.tvInstructions)
    TextView tvComments;

    @Bind(R.id.tvDecline)
    TextView tvDecline;

    @Bind(R.id.tvTextChoose)
    TextView tvTextChoose;

    @Bind(R.id.tvAccept)
    TextView tvAccept;

    @Bind(R.id.tvOr)
    TextView tvOr;

    private LatLng latLongDestination;
    private String orderId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_service);
        ButterKnife.bind(this);
        setTypeface();

        changeStatusBarColor();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.light_theme_color));
        }
    }

    private void setTypeface() {
        tvComments.setTypeface(AppGlobal.avn_medium);
        tvUserName.setTypeface(AppGlobal.avn_medium);
        tvServiceName.setTypeface(AppGlobal.avn_medium);
        tvDecline.setTypeface(AppGlobal.avn_medium);
        tvOr.setTypeface(AppGlobal.avn_medium);
        tvTextChoose.setTypeface(AppGlobal.avn_medium);
        tvAccept.setTypeface(AppGlobal.avn_bold);
    }

    private void initialise() {
        orderId = getIntent().getStringExtra(DataNames.ORDER_ID);
        getData(orderId);
    }

    private void getData(String id) {
        ProgressBarDialog.showProgressBar(this);
        Call<PojoOrderDetails> detailsCall = RestClient.getModalApiService().orderDetails(id);
        detailsCall.enqueue(new Callback<PojoOrderDetails>() {
            @Override
            public void onResponse(Call<PojoOrderDetails> call, Response<PojoOrderDetails> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    setData(response.body().data.get(0));
                    callDirectionsApi();
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), AcceptServiceActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoOrderDetails> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(AcceptServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setData(DataOrderDetais dataOrderDetais) {
        sdvUserImage.getHierarchy().setPlaceholderImage(ContextCompat.getDrawable(this, R.drawable.com_facebook_profile_picture_blank_portrait));
        if (dataOrderDetais.userId.profilePicURL.original != null) {
            sdvUserImage.setImageURI(dataOrderDetais.userId.profilePicURL.original);
        }
        tvUserName.setText(dataOrderDetais.userId.name);
        tvServiceName.setText(dataOrderDetais.details.get(0).productId.multiDetails.get(0).name);
        tvComments.setText(dataOrderDetais.comment);
        latLongDestination = new LatLng(dataOrderDetais.userId.lat, dataOrderDetais.userId.lng);
    }

    @OnClick({R.id.ivDriving, R.id.ivCycling, R.id.ivWalking, R.id.tvDecline})
    public void onCLick(View view) {
        switch (view.getId()) {
            case R.id.ivDriving:
                callApi(DataNames.ACCEPT, "driving", "accepted to serve");
                break;
            case R.id.ivCycling:
                callApi(DataNames.ACCEPT, "cycling", "accepted to serve");
                break;
            case R.id.ivWalking:
                callApi(DataNames.ACCEPT, "walking", "accepted to serve");
                break;
            case R.id.tvDecline:
                openReasonDialog();
                break;
        }
    }

    private void openReasonDialog() {
        final DialogReason dialog = new DialogReason(this, new DialogReason.OnSubmitClickListener() {
            @Override
            public void onButtonClick(String reason) {
                callApi(DataNames.DECLINE, "none", reason);
            }
        });
        dialog.show();
    }

    private void callApi(final String status, String trasportMode, String reason) {
        ProgressBarDialog.showProgressBar(this);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("orderId", orderId);
        hashMap.put("status", status);
        hashMap.put("transportMode", trasportMode);
        hashMap.put("reason", reason);
        Call<PojoSuccess> call = RestClient.getModalApiService().apiAcceptRejectService(StaticFunction.getAccessToken(this), hashMap);
        call.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (status.equals(DataNames.ACCEPT)) {
                        (AcceptServiceActivity.this).startActivity(new Intent(AcceptServiceActivity.this, StartServiceActivity.class)
                                .putExtra(DataNames.ORDER_ID, orderId));
                        (AcceptServiceActivity.this).overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                    } else {
                        onBackPressed();
                    }
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), AcceptServiceActivity.this, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(AcceptServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getMapImage(String encoded) {
        String[] markers = {"&markers=icon:http://54.148.76.216/resize/pin_pickup.png%7C" + latLongCurrentLocation.latitude + "," + latLongCurrentLocation.longitude,
                "&markers=icon:http://54.148.76.216/resize/pin_dropoff.png%7C" + latLongDestination.latitude + "," + latLongDestination.longitude};

        String uri = "https://maps.googleapis.com/maps/api/staticmap?" +
                "zoom=" + 13 +
                "&size=" + 300 + "x" + 300 +
                markers[0] + markers[1] +
                "&path=weight:3%7Ccolor:red%7Cenc:" + encoded +
                "&key=" + getString(R.string.g_api_key);
        sdvMapImage.setImageURI(uri);
    }

    private void callDirectionsApi() {
        String origin = latLongCurrentLocation.latitude + "," + latLongCurrentLocation.longitude;
        String destination = latLongDestination.latitude + "," + latLongDestination.longitude;
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("origin", origin);
        hashMap.put("destination", destination);
        hashMap.put("mode", "driving");
        hashMap.put("alternatives", "true");
        hashMap.put("sensor", "true");
        Call<PojoDirection> categoryCall = RestClient.getModalApiService().getDirections(hashMap);
        categoryCall.enqueue(new Callback<PojoDirection>() {
            @Override
            public void onResponse(Call<PojoDirection> call, Response<PojoDirection> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().routes.size() > 0) {
                        int shortestRoute = calculateShortestRoute(response.body().routes);
                        getMapImage(getEncodedString(response.body().routes.get(shortestRoute)));
                    }
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), AcceptServiceActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoDirection> call, Throwable t) {
                Toast.makeText(AcceptServiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String getEncodedString(RouteList routeList) {
        String encodedString;
        encodedString = routeList.overview_polyline.points;
        return encodedString;
    }

    private int calculateShortestRoute(List<RouteList> routes) {
        int shortestRoute = 0;
        int shortestDistance = routes.get(0).legs.get(0).distance.value;
        int distance;
        for (int i = 0; i < routes.size(); i++) {
            distance = routes.get(i).legs.get(0).distance.value;
            if (shortestDistance > distance) {
                shortestDistance = distance;
                shortestRoute = i;
            }
        }
        return shortestRoute;
    }


    @Override
    public void onLocationUpdated(LatLng latLongCurrentLocation) {
        super.onLocationUpdated(latLongCurrentLocation);
        this.latLongCurrentLocation = latLongCurrentLocation;
        stopLocationUpdates();
        initialise();
    }
}
