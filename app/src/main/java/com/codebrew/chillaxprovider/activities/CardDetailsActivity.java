package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
 * Created by cbl80 on 30/11/16.
 */
public class CardDetailsActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.tilAddress)
    TextInputLayout tilAddress;

    @Bind(R.id.etAddress)
    TextView etAddress;

    @Bind(R.id.tilAppNumber)
    TextInputLayout tilAppNumber;

    @Bind(R.id.etAppNumber)
    EditText etAppNumber;

    @Bind(R.id.tilAccessCode)
    TextInputLayout tilAccessCode;

    @Bind(R.id.etAccCode)
    EditText etAccCode;

    @Bind(R.id.tilPhoneNumber)
    TextInputLayout tilPhoneNumber;

    @Bind(R.id.etCvv)
    EditText etCvv;

    @Bind(R.id.tvContinue)
    TextView tvContinue;

    @Bind(R.id.tvText)
    TextView tvText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);
        ButterKnife.bind(this);
        settoolbar();
        setTypeface();
        changeStatusBarColor();
    }

    private void setTypeface() {
        tvTitle.setTypeface(AppGlobal.avn_medium);
        tvContinue.setTypeface(AppGlobal.avn_medium);
        tilAddress.setTypeface(AppGlobal.avn_roman);
        etAddress.setTypeface(AppGlobal.avn_roman);
        tilAppNumber.setTypeface(AppGlobal.avn_roman);
        etAppNumber.setTypeface(AppGlobal.avn_roman);
        tilAccessCode.setTypeface(AppGlobal.avn_roman);
        etAccCode.setTypeface(AppGlobal.avn_roman);
        tilPhoneNumber.setTypeface(AppGlobal.avn_roman);
        etCvv.setTypeface(AppGlobal.avn_roman);
        tvText.setTypeface(AppGlobal.avn_roman);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        }
    }


    private void settoolbar() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in_fast, R.anim.slide_right_out_fast);
    }

    @OnClick({R.id.tvContinue})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvContinue:
                startActivity(new Intent(CardDetailsActivity.this, HomeActivity.class));
                overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                break;
        }
    }
}
