package com.codebrew.chillaxprovider.activities;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.dialogs.GeneralDialog;

import java.io.IOException;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by cbl80 on 27/12/16.
 */
public class ReportActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.tvSubmit)
    TextView tvSubmit;

    @Bind(R.id.tv1)
    TextView tv1;

    @Bind(R.id.tv2)
    TextView tv2;

    @Bind(R.id.tv3)
    TextView tv3;

    @Bind(R.id.tv4)
    TextView tv4;

    @Bind(R.id.tv5)
    TextView tv5;

    @Bind(R.id.cbS1)
    CheckBox cbS1;

    @Bind(R.id.cbS2)
    CheckBox cbS2;

    @Bind(R.id.cbS3)
    CheckBox cbS3;

    @Bind(R.id.cbS4)
    CheckBox cbS4;

    @Bind(R.id.cbS5)
    CheckBox cbS5;

    @Bind(R.id.etFeedback)
    EditText etFeedback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        settoolbar();
        setTypeface();
        changeStatusBarColor();

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportApi();
            }
        });
    }

    private void reportApi() {
        String reportText="";
        if (cbS1.isChecked())
        {
            reportText=reportText+" "+tv1.getText().toString().trim();
        }
        if (cbS2.isChecked())
        {
            reportText=reportText+" "+tv2.getText().toString().trim();
        }
        if (cbS3.isChecked())
        {
            reportText=reportText+" "+tv3.getText().toString().trim();
        }
        if (cbS4.isChecked())
        {
            reportText=reportText+" "+tv4.getText().toString().trim();
        }
        if (cbS5.isChecked())
        {
            reportText=reportText+" "+tv5.getText().toString().trim();
        }

        if (etFeedback.getText().toString().trim().length()>0)
        {
            reportText=reportText+" "+etFeedback.getText().toString().trim();
        }
        if (reportText.length()>0)
        {
            reportApiHit(reportText);
        }else
        {
            Toast.makeText(this,getString(R.string.error),Toast.LENGTH_LONG).show();
        }
    }

    private void reportApiHit(String reportText) {
        ProgressBarDialog.showProgressBar(this);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("orderId",getIntent().getStringExtra(DataNames.ORDER_ID));
        hashMap.put("text",reportText);
        Call<PojoSuccess> successCall= RestClient.getModalApiService().reportOrder(StaticFunction.getAccessToken(this),
                hashMap);

        successCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    showDialog();
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), ReportActivity.this
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(ReportActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void showDialog() {
        GeneralDialog generalDialog = new GeneralDialog(this, new GeneralDialog.OnOkClickListener() {
            @Override
            public void onButtonClick() {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 300);
            }
        }, getString(R.string.feedback_thnks), "");
        generalDialog.show();
    }
    private void setTypeface() {
        tvTitle.setTypeface(AppGlobal.avn_medium);
        tvSubmit.setTypeface(AppGlobal.avn_medium);
        tv1.setTypeface(AppGlobal.avn_medium);
        tv2.setTypeface(AppGlobal.avn_medium);
        tv3.setTypeface(AppGlobal.avn_medium);
        tv4.setTypeface(AppGlobal.avn_medium);
        tv5.setTypeface(AppGlobal.avn_medium);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.dark_theme_color));
        }
    }

    private void settoolbar() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in_fast, R.anim.slide_right_out_fast);
    }
}
