package com.codebrew.chillaxprovider.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.activities.SplashActivity;
import com.codebrew.chillaxprovider.models.Data;
import com.codebrew.chillaxprovider.models.DataLanguage;
import com.codebrew.chillaxprovider.models.PojoAllLanguage;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/*
 * Created by cbl45 on 7/5/16.
 */
public class StaticFunction {

    public static String getAccessToken(Context context) {
        String token = "";
        Data pojoLogin = Prefs.with(context).getObject(DataNames.USER_DATA, Data.class);
        if (pojoLogin != null && pojoLogin.accessToken != null)
            token = "bearer " + pojoLogin.accessToken;
        return token;
    }

    public static String getLanguageId(Context context) {
        List<DataLanguage> data;
        PojoAllLanguage dataL = Prefs.with(context).getObject(DataNames.LANGUAGES, PojoAllLanguage.class);
        data = dataL.data;
        return data.get(0)._id;
    }

    public static boolean loginProper(Context context) {
        Data pojoLogin = Prefs.with(context).getObject(DataNames.USER_DATA, Data.class);
        return pojoLogin != null && pojoLogin.accessToken != null && pojoLogin.phoneVerifiy && pojoLogin.categorySelect;
    }

    public static void handleError(String s, final Context context, int statusCode) {
        if (statusCode == 401) {
            Toast.makeText(context, context.getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Prefs.with(context).removeAll();
                    ((Activity) context).finishAffinity();
                    Intent intent = new Intent(context, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("extra_show_animation", false);
                    context.startActivity(intent);
                }
            }, 500);
        } else {
            try {
                JSONObject jsonObject = new JSONObject(s);
                String message = jsonObject.getString("message");
                if (jsonObject.has("type")) {
                    String type = jsonObject.getString("type");
                    switch (type) {
                        case "EMAIL_EXIST":
                            message = context.getString(R.string.email_exist);
                            break;
                        case "PHONE_NO_EXIST":
                            message = context.getString(R.string.phone_exist);
                            break;
                        case "IMP_ERROR":
                            message = context.getString(R.string.imp_error);
                            break;
                        case "INVALID_USER_PASS":
                            message = context.getString(R.string.invalid_user_pass);
                            break;
                    }
                }
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public static void showLogoutAlert(final Context context) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.log_out_t));
        alertDialog.setCancelable(false);
        alertDialog.setMessage(context.getString(R.string.logout_message));
        alertDialog.setPositiveButton(context.getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, int which) {
                        LoginManager.getInstance().logOut();
                        Prefs.with(context).removeAll();
                        ((Activity) context).finishAffinity();
                        Intent intent = new Intent(context, SplashActivity.class);
                        intent.putExtra("extra_show_animation", false);
                        context.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(ContextCompat.getColor(context, R.color.dark_theme_color));
        nbutton.setTypeface(AppGlobal.avn_roman);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(ContextCompat.getColor(context, R.color.dark_theme_color));
        pbutton.setTypeface(AppGlobal.avn_roman);
        TextView textView = (TextView) alert.findViewById(android.R.id.message);
        assert textView != null;
        textView.setTypeface(AppGlobal.avn_book);
    }
}