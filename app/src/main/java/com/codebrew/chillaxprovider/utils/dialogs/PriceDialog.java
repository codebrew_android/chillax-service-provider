package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;

/*
 * Created by cbl80 on 24/11/16.
 */
public class PriceDialog extends Dialog {


    public OnOkClickListener mListener;
    private Context context;


    public PriceDialog(Context context
            , OnOkClickListener onClick, int count) {
        super(context, R.style.TransparentDilaog);
        mListener = onClick;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_number_people);
        setCancelable(true);
        LinearLayout rlContainer = (LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width = GeneralFunctions.getScreenWidth(context) - 60;
        TextView tvOk = (TextView) findViewById(R.id.tvSubmit);
        tvOk.setTypeface(AppGlobal.avn_medium);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setTypeface(AppGlobal.avn_book);
        final EditText editText = (EditText) findViewById(R.id.etPrice);
        editText.setTypeface(AppGlobal.avn_bold);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    if (editText.getText().toString().trim().length() != 0) {
                        dismiss();
                        mListener.onButtonClick(editText.getText().toString().trim());
                    } else {
                        editText.setError(context.getString(R.string.eneter_price));
                        editText.requestFocus();
                    }
                }
            }


        });


    }


    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnOkClickListener {
        void onButtonClick(String count);
    }
}