package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;

/*
 * Created by cbl96 on 20/12/16.
 */

public class DialogPopMessage extends Dialog{

    DialogPopMessage.OnButtonClickListener onButtonClick;
    Context context;

    public DialogPopMessage(Context context, DialogPopMessage.OnButtonClickListener onButtonClick) {
        super(context);
        this.context = context;
        this.onButtonClick = onButtonClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dialog_popup);
        setCancelable(true);
        LinearLayout rlContainer=(LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width= GeneralFunctions.getScreenWidth(context)-60;

        TextView tvText1 = (TextView) findViewById(R.id.tvText1);
        TextView tvText2= (TextView) findViewById(R.id.tvText2);

        SpannableStringBuilder  spannableStringBuilder = new SpannableStringBuilder(tvText2.getText().toString());
        String selectedString = context.getString(R.string.end_service);

        int start = spannableStringBuilder.toString().indexOf(selectedString);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context,R.color.colorAccent)),start,start+selectedString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvText2.setText(spannableStringBuilder);
        tvText1.setTypeface(AppGlobal.avn_medium);
        tvText2.setTypeface(AppGlobal.avn_medium);

        tvText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onButtonClick != null) {
                    dismiss();
                    onButtonClick.onButtonClick();
                }
            }
        });

    }

    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnButtonClickListener {
        void onButtonClick();
    }

}