package com.codebrew.chillaxprovider.utils;

import android.graphics.Typeface;
import android.support.multidex.MultiDexApplication;

import com.facebook.drawee.backends.pipeline.Fresco;

/*
 * Created by cbl80 on 21/11/16.
 */
public class AppGlobal extends MultiDexApplication {

    public static Typeface avn_medium, avn_book, avn_heavy, avn_roman,avn_bold;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(getApplicationContext());
        avn_medium = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Medium.otf");
        avn_book = Typeface.createFromAsset(getAssets(), "fonts/AvenirNextLTPro-Regular.otf");
        avn_heavy = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Heavy.otf");
        avn_roman = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Roman.otf");
        avn_bold = Typeface.createFromAsset(getAssets(), "fonts/AvenirNextLTPro-Bold.otf");

    }
}
