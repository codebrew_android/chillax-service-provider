package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;


/*
 * Created by cbl80 on 18/5/16.
 */
public class PhoneNumberDialog extends Dialog {


    public OnOkClickListener mListener;
    private Context context;
    private EditText etPhoneNumber;


    public PhoneNumberDialog(Context context
            , OnOkClickListener onClick) {
        super(context, R.style.TransparentDilaog);
        mListener = onClick;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_phone_number);
        setCancelable(true);
        LinearLayout rlContainer = (LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width = GeneralFunctions.getScreenWidth(context) - 60;
        TextView tvOk = (TextView) findViewById(R.id.tvOk);
        tvOk.setTypeface(AppGlobal.avn_medium);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        etPhoneNumber.setTypeface(AppGlobal.avn_book);
        TextView tvOtpString = (TextView) findViewById(R.id.tvOtpString);
        tvOtpString.setTypeface(AppGlobal.avn_medium);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setTypeface(AppGlobal.avn_book);
        final TextView tvCountryCode = (TextView) findViewById(R.id.tvCountryCode);
        tvCountryCode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.flag_ca, 0, 0, 0);
        tvCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CountryPicker picker = CountryPicker.newInstance(context.getString(R.string.select_coutry));
                picker.show(((AppCompatActivity) context).getSupportFragmentManager(), context.getString(R.string.country_picker));
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        // Implement your code here
                        tvCountryCode.setText(dialCode);
                        tvCountryCode.setCompoundDrawablesWithIntrinsicBounds(flagDrawableResID, 0, 0, 0);
                        picker.dismiss();
                    }
                });
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    if (etPhoneNumber.getText().toString().trim().length()>8) {
                        dismiss();
                        mListener.onButtonClick(etPhoneNumber.getText().toString().trim(),
                                tvCountryCode.getText().toString());
                    }else if (etPhoneNumber.getText().toString().trim().length()==0)
                    {
                        etPhoneNumber.requestFocus();
                        etPhoneNumber.setError(context.getString(R.string.error_empty_phone_number));
                    }else
                    {
                        etPhoneNumber.setError(context.getString(R.string.error_phone_number));
                    }
                }
            }


        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                GeneralFunctions.hideKeyboard(etPhoneNumber, context);
            }
        });

    }


    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnOkClickListener {
        void onButtonClick(String number, String countryCode);
    }
}
