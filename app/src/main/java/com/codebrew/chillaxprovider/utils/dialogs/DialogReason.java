package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;

/**
 * Created by cbl96 on 19/12/16.
 */
public class DialogReason extends Dialog {

    OnSubmitClickListener onSubmitClickListener;
    private Context context;

    public DialogReason(Context context,OnSubmitClickListener onSubmitClickListener) {
        super(context, R.style.TransparentDilaog);
        this.onSubmitClickListener = onSubmitClickListener;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dialog_reason);
        setCancelable(true);
        LinearLayout rlContainer=(LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width=GeneralFunctions.getScreenWidth(context)-60;


        TextView tvSubmit = (TextView) findViewById(R.id.tvSubmit);
        final TextView tvTextReason = (TextView) findViewById(R.id.tvTextReason);
        final EditText etReason = (EditText) findViewById(R.id.etReason);

        etReason.setTypeface(AppGlobal.avn_medium);
        tvTextReason.setTypeface(AppGlobal.avn_medium);
        tvSubmit.setTypeface(AppGlobal.avn_medium);

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSubmitClickListener != null) {
                    dismiss();
                    onSubmitClickListener.onButtonClick(etReason.getText().toString().trim());
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                GeneralFunctions.hideKeyboard(etReason, context);
            }
        });
    }


    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnSubmitClickListener {
        void onButtonClick(String reason);
    }

}