package com.codebrew.chillaxprovider.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;


import com.codebrew.chillaxprovider.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


/*
 * Created by Ankit Jindal on 7/23/2015.
 */
public class GeneralFunctions {

    public static final String JPEG_FILE_PREFIX = "IMG_";
    public static final String JPEG_FILE_SUFFIX = ".jpg";

    public static String getFormattedPager(String dob) {
        String date = "";
        try {
            date = new SimpleDateFormat("dd/MM", Locale.US).format(new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(dob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String date1;
        Calendar calendar = Calendar.getInstance();
        date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(calendar.getTime());
        if (date1.equals(dob)) {
            date = "Today";
        }
        return date;
    }

    public static String getComparePager(String dob) {
        String date = dob;
        try {
            date = new SimpleDateFormat("EEEE hh:mm a", Locale.US).format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US).parse(dob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }


    public static void updatePlayServices(Activity activity) {
        final String appPackageName = "com.google.android.gms";
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }


    public static void hideKeyboard(View view, Context context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static String getUtcDate(String dob) {
        try {
            Date date = new SimpleDateFormat("dd MMM yyyy", Locale.US).parse(dob);
            SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            f.setTimeZone(TimeZone.getDefault());
            return f.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getUtcTime(String dob) {
        try {
            Date date = new SimpleDateFormat("hh:mm a", Locale.US).parse(dob);
            SimpleDateFormat f = new SimpleDateFormat("hh:mm a", Locale.US);
            f.setTimeZone(TimeZone.getDefault());
            return f.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long getDifference(String dob) {
        long l = 0;
        try {
            Date date = new SimpleDateFormat("MMM dd,yyyy hh:mm a", Locale.US).parse(dob);
            l = SystemClock.elapsedRealtime() - date.getTime();
            Date now = new Date();
            long elapsedTime = now.getTime() - date.getTime(); //startTime is whatever time you want to start the chronometer from. you might have stored it somwehere
            l = SystemClock.elapsedRealtime() - elapsedTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return l;
    }


    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                .matches();
    }


    public static int getScreenWidth(Context activity) {
        WindowManager w = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        w.getDefaultDisplay().getSize(size);
        return size.x;

    }

    public static int getScreenHeight(Context activity) {
        WindowManager w = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);

        Point size = new Point();
        w.getDefaultDisplay().getSize(size);
        return size.y;

    }


    public static File setUpImageFile(String directory) throws IOException {
        File imageFile = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())) {
            File storageDir = new File(directory);
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d("CameraSample", "failed to create directory");
                    return null;
                }
            }

            imageFile = File.createTempFile(JPEG_FILE_PREFIX
                            + System.currentTimeMillis() + "_",
                    JPEG_FILE_SUFFIX, storageDir);
        }
        return imageFile;
    }

    public static String getCurrentDateBooking() {
        String date;
        Calendar calendar = Calendar.getInstance();
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(calendar.getTime());
        return date;
    }

    public static String getCurrentDate() {
        String date;
        Calendar calendar = Calendar.getInstance();
        date = new SimpleDateFormat("dd MMM yyyy", Locale.US).format(calendar.getTime());
        return date;
    }

    public static String getCurrentTime() {
        String date;
        Calendar calendar = Calendar.getInstance();
        date = new SimpleDateFormat("hh:mm aaa", Locale.US).format(calendar.getTime());
        return date;
    }



    public static String getduration(String sd) {


        Date startDate = null;
        try {
            startDate = new SimpleDateFormat("MMM dd,yyyy hh:mm aaa", Locale.US).parse(sd);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert startDate != null;
        long different = System.currentTimeMillis() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different / minutesInMilli;

        long elapsedSec = different / secondsInMilli;

        return String.valueOf(elapsedHours) + ":" + elapsedMinutes + ":" + elapsedSec;

    }


    @SuppressLint("SimpleDateFormat")
    public static long getDateDifference(String sd) {


        Date startDate = null;
        try {
            startDate = new SimpleDateFormat("MMM dd,yyyy hh:mm aaa", Locale.US).parse(sd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long different;
        assert startDate != null;
        if (System.currentTimeMillis() < startDate.getTime()) {
            different = startDate.getTime() - System.currentTimeMillis();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            different = different / minutesInMilli;
        } else
            different = 0;

        return different;

    }

    @SuppressLint("SimpleDateFormat")
    public static int printDifference(String sd) {


        Date startDate = null;
        try {
            startDate = new SimpleDateFormat("MMM dd,yyyy hh:mm aaa", Locale.US).parse(sd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long different;
        assert startDate != null;
        different = System.currentTimeMillis() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;

        if (elapsedMinutes < 2 && elapsedHours > 0) {
            return (int) elapsedHours;
        } else {
            return (int) (elapsedHours) + 1;
        }

    }

    public static void openPlayStore(Context context)
    {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
        ((Activity)context).overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
    }
    public static int getVersion(Context context) {
        PackageManager manager = context.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public static List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }
}