package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;

/**
 * Created by cbl96 on 20/12/16.
 */

public class DialogCancellation extends Dialog{

    OnButtonClickListener onButtonClick;
    Context context;

    public DialogCancellation(Context context, OnButtonClickListener onButtonClick) {
        super(context);
        this.context = context;
        this.onButtonClick = onButtonClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_confirm_cancellation);
        setCancelable(true);
        LinearLayout rlContainer=(LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width= GeneralFunctions.getScreenWidth(context)-60;

        TextView tvTextCancel = (TextView) findViewById(R.id.tvTextCancel);
        TextView tvConditions = (TextView) findViewById(R.id.tvConditions);
        TextView tvYes = (TextView) findViewById(R.id.tvYes);
        TextView tvNo= (TextView) findViewById(R.id.tvNo);

        tvTextCancel.setTypeface(AppGlobal.avn_roman);
        tvConditions.setTypeface(AppGlobal.avn_roman);
        tvYes.setTypeface(AppGlobal.avn_roman);
        tvNo.setTypeface(AppGlobal.avn_roman);

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onButtonClick != null) {
                    dismiss();
                    onButtonClick.onButtonYesClick();
                }
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onButtonClick != null) {
                    dismiss();
                }
            }
        });

    }


    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnButtonClickListener {
        void onButtonYesClick();
    }

}