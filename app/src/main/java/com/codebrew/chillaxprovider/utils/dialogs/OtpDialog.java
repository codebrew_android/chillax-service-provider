package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;


/*
 * Created by cbl80 on 22/11/16.
 */
public class OtpDialog extends Dialog {


    public OnOkClickListener mListener;
    private Context context;
    private EditText etPhoneNumber;
    private int otp;


    public OtpDialog(Context context
            , OnOkClickListener onClick, Integer otp) {
        super(context, R.style.TransparentDilaog);
        mListener = onClick;
        this.context = context;
        this.otp = otp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_otp);
        setCancelable(true);
        LinearLayout rlContainer = (LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width = GeneralFunctions.getScreenWidth(context) - 60;
        TextView tvOk = (TextView) findViewById(R.id.tvOk);
        tvOk.setTypeface(AppGlobal.avn_medium);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        etPhoneNumber.setTypeface(AppGlobal.avn_book);
        etPhoneNumber.setText(String.valueOf(otp));
        TextView tvOtpString = (TextView) findViewById(R.id.tvOtpString);
        tvOtpString.setTypeface(AppGlobal.avn_medium);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setTypeface(AppGlobal.avn_book);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    if (etPhoneNumber.getText().toString().trim().length() > 0) {
                        mListener.onButtonClick(etPhoneNumber.getText().toString().trim());
                    } else {
                        etPhoneNumber.requestFocus();
                        etPhoneNumber.setError(context.getString(R.string.error_empty_phone_number));
                    }
                }
            }


        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                GeneralFunctions.hideKeyboard(etPhoneNumber, context);
            }
        });

    }


    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnOkClickListener {
        void onButtonClick(String otp);
    }
}