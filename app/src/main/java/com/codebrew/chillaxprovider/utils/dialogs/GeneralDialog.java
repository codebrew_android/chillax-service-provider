package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;

/*
 * Created by cbl80 on 15/12/16.
 */
public class GeneralDialog extends Dialog {


    public OnOkClickListener mListener;
    private Context context;
    private String message, title;


    public GeneralDialog(Context context, OnOkClickListener onClick, String title, String message) {
        super(context, R.style.TransparentDilaog);
        mListener = onClick;
        this.context = context;
        this.message = message;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_general_dialog);
        setCancelable(false);
        LinearLayout rlContainer = (LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width = GeneralFunctions.getScreenWidth(context)-60;
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setTypeface(AppGlobal.avn_heavy);

        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        tvMessage.setTypeface(AppGlobal.avn_medium);

        tvTitle.setText(title);
        tvMessage.setText(message);
        rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    dismiss();
                    mListener.onButtonClick();
                }
            }
        });

    }

    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnOkClickListener {
        void onButtonClick();
    }
}