package com.codebrew.chillaxprovider.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.widget.ProgressBar;

import com.codebrew.chillaxprovider.R;

public class ProgressBarDialog {


    private static Dialog dialog;

    public static void showProgressBar(final Context activity) {

        if (dialog != null) {
            if (dialog.isShowing())
                return;
        }

        try {

            dialog= new Dialog(activity, R.style.CustomDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.layout_progressbar_dialog);
            ProgressBar pbHeaderProgress=(ProgressBar)dialog.findViewById(R.id.pbHeaderProgress);
            pbHeaderProgress.getIndeterminateDrawable().
                    setColorFilter(ContextCompat.getColor(activity,R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public static void dismissProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                try {
                    dialog.dismiss();
                }catch (Exception | Error e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

}
/*
package com.codebrew.chillaxprovider.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.WindowManager;

import com.codebrew.chillaxprovider.R;


public class ProgressBarDialog {


    private static Dialog dialog;

    public static void showProgressBar(final Context activity) {

        if (dialog != null) {
            if (dialog.isShowing()) {
                return;
            }
        }

        try {
            // if ("".equalsIgnoreCase(title)) {
            // title = "Alert";
            // }
            dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog_AppCompat;
            dialog.setContentView(R.layout.progressbar_dialog);

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public static void dismissProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                try {
                    dialog.dismiss();
                }catch (Exception | Error e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

}
*/
