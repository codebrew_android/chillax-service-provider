package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;


/*
 * Created by cbl80 on 14/6/16.
 */
public class ForgotPasswordDialog extends Dialog {


    public OnOkClickListener mListener;
    private Context context;


    public ForgotPasswordDialog(Context context, OnOkClickListener onClick) {
        super(context, R.style.TransparentDilaog);
        mListener = onClick;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_forgot_password);
        setCancelable(false);
        LinearLayout rlContainer=(LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().height= GeneralFunctions.getScreenHeight(context);
        rlContainer.getLayoutParams().width=GeneralFunctions.getScreenWidth(context);


        ImageView ivCross = (ImageView) findViewById(R.id.ivBack);
        TextView tvTitle = (TextView) findViewById(R.id.tvForgotPassword);
        tvTitle.setTypeface(AppGlobal.avn_medium);

        TextView tvDesc=(TextView)findViewById(R.id.tvDesc);
        tvDesc.setTypeface(AppGlobal.avn_roman);

        assert ivCross != null;
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        Button tvGo = (Button) findViewById(R.id.tvSubmit);

        tvGo.setTypeface(AppGlobal.avn_bold);

        final EditText etSearch = (EditText) findViewById(R.id.etEmail);

        etSearch.setTypeface(AppGlobal.avn_roman);


        tvGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralFunctions.hideKeyboard(etSearch, context);

                if (mListener != null) {
                    if (GeneralFunctions.isValidEmail(etSearch.getText().toString().trim().toLowerCase())) {
                        mListener.onButtonClick(etSearch.getText().toString().trim());
                        dismiss();
                    } else {
                        etSearch.setError(context.getString(R.string.invalid_email));
                    }

                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                GeneralFunctions.hideKeyboard(etSearch, context);
            }
        });

    }

    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnOkClickListener {
        void onButtonClick(String emailId);
    }
}