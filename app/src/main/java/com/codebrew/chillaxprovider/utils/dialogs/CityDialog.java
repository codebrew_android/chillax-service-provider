package com.codebrew.chillaxprovider.utils.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.CityAdapter;
import com.codebrew.chillaxprovider.models.PojoCity;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.codebrew.chillaxprovider.utils.RecyclerItemClickListener;

/*
 * Created by cbl80 on 26/12/16.
 */
public class CityDialog extends Dialog {


    public OnOkClickListener mListener;
    private Context context;


    public CityDialog(Context context
            , OnOkClickListener onClick) {
        super(context, R.style.TransparentDilaog);
        mListener = onClick;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_city);
        setCancelable(true);
        LinearLayout rlContainer = (LinearLayout) findViewById(R.id.rlContainer);
        rlContainer.getLayoutParams().width = GeneralFunctions.getScreenWidth(context) - 60;
        RecyclerView rvCity=(RecyclerView) findViewById(R.id.rvCity);
        rvCity.setLayoutManager(new LinearLayoutManager(context));
        final PojoCity pojoCity= Prefs.with(context).getObject(DataNames.CITY_LIST,PojoCity.class);
        final CityAdapter cityAdapter=new CityAdapter(context,pojoCity.data);
        rvCity.setAdapter(cityAdapter);

        rvCity.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                if (mListener!=null)
                {
                    mListener.onButtonClick(pojoCity.data.get(position).multiDetails.get(0)
                    .name,pojoCity.data.get(position)._id);
                    dismiss();
                }
            }

            @Override
            public void onItemLongPress(View childView, int position) {

            }
        }));
    }


    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        super.setOnKeyListener(onKeyListener);
    }


    public interface OnOkClickListener {
        void onButtonClick(String number, String countryCode);
    }
}