package com.codebrew.chillaxprovider.utils;

import android.content.IntentFilter;

/*
 * Created by cbl80 on 21/9/16.
 */
public class DataNames {

    public static final String TAB1 = "HOME";
    public static final String TAB2 = "REST";
    public static final String TAB3 = "IRONDETAILPICKUP";

    public static final String PUSH_TOKEN = "Push_token";
    public static final String USER_DATA = "user_data";
    public static final String LANGUAGES = "languages";
    public static final String USER_PROFILE = "user_profile";
    public static final String IS_ARRVIED = "is_arrived";
    public static final String IS_START = "is_start";
    public static final String ORDER_ID = "order_id";
    public static final String ACCEPT = "ACCEPT";
    public static final String DECLINE = "REJECT";
    public static final String INTENT_REFRESH_FROM_NOTIFICATION ="intent_from_notification_refersh" ;
    public static final String SUPPLIER_ID = "supplier_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String CITY_LIST = "city_list";
}
