package com.codebrew.chillaxprovider.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Rest client
 */
public class RestClient {
    private static Api apiServiceModel = null;

    /**
     * By using this method your response will be coming in  modal class object
     *
     * @return object of ApiService interface
     */
    public static Api getModalApiService() {
        if (apiServiceModel == null) {
            OkHttpClient okHttpClient = new OkHttpClient();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Config.getBaseURL())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient.newBuilder().connectTimeout(60,TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
                    .build();

            apiServiceModel =
                    retrofit.create(Api.class);
        }
        return apiServiceModel;
    }
}