package com.codebrew.chillaxprovider.retrofit;


import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/*
 * Created by Ankit jindal on 19/12/2015.
 */
public class RetrofitUtils {

    public static RequestBody stringToRequestBody(String string) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), string);
    }

    public static MultipartBody.Part imageToRequestBody(File file, String fieldName) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(fieldName, file.getName(), requestFile);
    }




}

