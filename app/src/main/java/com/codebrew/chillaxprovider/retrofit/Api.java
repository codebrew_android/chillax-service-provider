package com.codebrew.chillaxprovider.retrofit;


import com.codebrew.chillaxprovider.models.PojoAllLanguage;
import com.codebrew.chillaxprovider.models.PojoCategoryListing;
import com.codebrew.chillaxprovider.models.PojoCity;
import com.codebrew.chillaxprovider.models.PojoEarnings;
import com.codebrew.chillaxprovider.models.PojoOrderDetails;
import com.codebrew.chillaxprovider.models.PojoPendingOrder;
import com.codebrew.chillaxprovider.models.PojoProfile;
import com.codebrew.chillaxprovider.models.PojoSignUp;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.models.pojodirection.PojoDirection;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface Api {

    /*1*/
    @FormUrlEncoded
    @POST("/api/supplier/signUp")
    Call<PojoSignUp> signUp(@FieldMap HashMap<String, String> hashMap);

    /*2*/
    @GET("/api/supplier/verifiyOtp")
    Call<PojoSuccess> verifyOtp(@Header("authorization") String token, @Query("otp") String otp);

    /*3*/
    @GET("/api/supplier/forgetPassword")
    Call<PojoSuccess> forgotPassword(@Query("email") String email);

    /*4*/
    @FormUrlEncoded
    @POST("/api/supplier/login")
    Call<PojoSignUp> login(@FieldMap HashMap<String, String> hashMap);

    /*5*/
    @FormUrlEncoded
    @POST("/api/supplier/loginwithFb")
    Call<PojoSignUp> loginWithFB(@FieldMap HashMap<String, String> hashMap);

    /*6*/
    @FormUrlEncoded
    @POST("/api/supplier/loginwithGmail")
    Call<PojoSignUp> loginWithGoogle(@FieldMap HashMap<String, String> hashMap);

    /*7*/
    @GET("/api/supplier/getAllLanguage")
    Call<PojoAllLanguage> getAllLanguage();

    /*8*/
    @FormUrlEncoded
    @PUT("/api/supplier/updatePhoneNo")
    Call<PojoSignUp> phoneNumberUpdate(@Header("authorization") String token, @FieldMap HashMap<String, String> hashMap);

    /*9*/
    @GET("/api/supplier/listing")
    Call<PojoCategoryListing> categoryListing(@Header("authorization") String token, @Query("languageId") String langId);

    /*10*/
    @FormUrlEncoded
    @POST("/api/supplier/enterPrice")
    Call<PojoSuccess> enterPrice(@Header("authorization") String token, @FieldMap HashMap<String, String> hashMap);

    /*11*/
    @GET("/api/supplier/supplierProfile")
    Call<PojoProfile> getProfile(@Header("authorization") String token);

    /*12*/
    @FormUrlEncoded
    @PUT("/api/supplier/vendorStatus")
    Call<PojoSuccess> statusUpdate(@Header("authorization") String token, @Field("status") boolean status);

    /*13*/
    @GET("https://maps.googleapis.com/maps/api/directions/json")
    Call<PojoDirection> getDirections(@QueryMap HashMap<String, String> hashMap);

    /*14*/
    @GET("/api/supplier/pendingOrder")
    Call<PojoPendingOrder> getPendingOrder(@Header("authorization") String token);

    /*15*/
    @FormUrlEncoded
    @POST("/api/supplier/orderStatus")
    Call<PojoSuccess> apiAcceptRejectService(@Header("authorization") String token, @FieldMap HashMap<String, String> hashMap);

    /*16*/
    @FormUrlEncoded
    @POST("/api/supplier/arrived")
    Call<PojoSuccess> apiArrived(@Header("authorization") String token, @FieldMap HashMap<String, String> hashMap);

    /*17*/
    @FormUrlEncoded
    @POST("/api/supplier/cancelService")
    Call<PojoPendingOrder> apiCancelService(@Header("authorization") String token, @FieldMap HashMap<String, String> hashMap);

    /*18*/
    @FormUrlEncoded
    @POST("/api/supplier/updateLocation")
    Call<PojoSuccess> apiUpdateLocation(@Header("authorization") String token, @FieldMap HashMap<String, String> hashMap);

    /*19*/
    @FormUrlEncoded
    @POST("/api/supplier/endService")
    Call<PojoSuccess> endService(@Header("authorization") String token,@Field("orderId") String hashMap);

    /*20*/
    @GET("/api/supplier/orderDeatils")
    Call<PojoOrderDetails> orderDetails(@Query("orderId") String hashMap);

    /*21*/
    @FormUrlEncoded
    @POST("/api/supplier/rateUser")
    Call<PojoSuccess> rateService(@Header("authorization") String token,@FieldMap HashMap<String,String> hashMap);

    /*22*/
    @GET("/api/users/getAllCity")
    Call<PojoCity> cityList(@Query("languageId")String languageId);

    /*23*/
    @FormUrlEncoded
    @POST("/api/supplier/report")
    Call<PojoSuccess> reportOrder(@Header("authorization") String token,@FieldMap HashMap<String,String> hashMap);

    /*24*/
    @POST("/api/supplier/report")
    Call<PojoEarnings> earnings(@Header("authorization") String token);

}