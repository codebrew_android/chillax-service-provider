package com.codebrew.chillaxprovider.retrofit;


/**
 * Created by Harman Sandhu
 */
public class Config {

    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */


    static String BASE_URL = "";
    static AppMode appMode = AppMode.DEV;


    static public String getBaseURL() {
        init(appMode);
        return BASE_URL;
    }


    public static void init(AppMode appMode) {

        switch (appMode) {

            case TEST:
                BASE_URL = "http://192.168.100.83:8000";
                break;

            case DEV:
                BASE_URL = "http://192.168.100.111:8000";
                break;

            case LIVE:
                BASE_URL = "http://35.160.140.107:8000";
                break;
        }
    }


    public enum AppMode {
        DEV, TEST, LIVE
    }

}