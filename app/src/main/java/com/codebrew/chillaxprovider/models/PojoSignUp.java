package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoSignUp {

@SerializedName("statusCode")
@Expose
public Integer statusCode;
@SerializedName("message")
@Expose
public String message;
@SerializedName("data")
@Expose
public DataSignUp data;

}