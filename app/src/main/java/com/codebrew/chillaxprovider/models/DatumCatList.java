package com.codebrew.chillaxprovider.models;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl80 on 8/12/16.
 */
public class DatumCatList implements ParentListItem{


    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("subCategoryList")
    @Expose
    public List<SubCategoryList> subCategoryList = new ArrayList<>();
    @SerializedName("thirdLevel")
    @Expose
    public Boolean thirdLevel;
    @SerializedName("multiDetails")
    @Expose
    public List<MultiDetail> multiDetails = new ArrayList<>();
    @SerializedName("isBlocked")
    @Expose
    public Boolean isBlocked;
    @SerializedName("isDeleted")
    @Expose
    public Boolean isDeleted;
    @SerializedName("image")
    @Expose
    public ProfilePicURL image;
    @SerializedName("__v")
    @Expose
    public Integer __v;


    @Override
    public List<?> getChildItemList() {
        return subCategoryList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
