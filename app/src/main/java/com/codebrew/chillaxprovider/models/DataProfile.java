package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl80 on 8/12/16.
 */
public class DataProfile {
    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("reviews")
    @Expose
    public Integer reviews;
    @SerializedName("ratings")
    @Expose
    public Float ratings=0f;
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("categories")
    @Expose
    public List<Category> categories = new ArrayList<>();
    @SerializedName("profilePicURL")
    @Expose
    public ProfilePicURL profilePicURL;
    @SerializedName("name")
    @Expose
    public String name;
}
