package com.codebrew.chillaxprovider.models;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
 * Created by cbl80 on 8/12/16.
 */
public class ProductList {
    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("__v")
    @Expose
    public Integer __v;
    @SerializedName("multiDetails")
    @Expose
    public List<MultiDetail> multiDetails = null;
    @SerializedName("DetailsSubCategoires")
    @Expose
    public Object detailsSubCategoires;
    @SerializedName("subcategoryId")
    @Expose
    public String subcategoryId;
    @SerializedName("categoryId")
    @Expose
    public String categoryId;
    @SerializedName("isBlocked")
    @Expose
    public Boolean isBlocked;
    @SerializedName("isDeleted")
    @Expose
    public Boolean isDeleted;
    @SerializedName("image")
    @Expose
    public ProfilePicURL image;

}
