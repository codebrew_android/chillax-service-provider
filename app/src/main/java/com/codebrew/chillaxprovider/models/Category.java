package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

@SerializedName("_id")
@Expose
public String _id;
@SerializedName("isDeleted")
@Expose
public Boolean isDeleted;
@SerializedName("price")
@Expose
public String price;
@SerializedName("isApproved")
@Expose
public Boolean isApproved;
@SerializedName("registrationDate")
@Expose
public String registrationDate;
@SerializedName("productId")
@Expose
public String productId;
@SerializedName("subCategoryId")
@Expose
public String subCategoryId;
@SerializedName("categoryId")
@Expose
public CategoryId categoryId;

}