package com.codebrew.chillaxprovider.models.pojodirection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl89 on 4/10/16.
 */
public class PojoDirection {
    @SerializedName("geocoded_waypoints")
    @Expose
    public List<Geocoded_waypoint> geocoded_waypoints = new ArrayList<>();
    @SerializedName("routes")
    @Expose
    public List<RouteList> routes = new ArrayList<RouteList>();
    @SerializedName("status")
    @Expose
    public String status;
}
