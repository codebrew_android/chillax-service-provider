package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl80 on 26/12/16.
 */
public class DataOrderDetais {
    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("reason")
    @Expose
    public String reason;
    @SerializedName("transportMode")
    @Expose
    public String transportMode;
    @SerializedName("supplierReportText")
    @Expose
    public String supplierReportText;
    @SerializedName("supplierReport")
    @Expose
    public Boolean supplierReport;
    @SerializedName("reportText")
    @Expose
    public String reportText;
    @SerializedName("report")
    @Expose
    public Boolean report;
    @SerializedName("details")
    @Expose
    public List<Detail> details = new ArrayList<>();
    @SerializedName("netAmount")
    @Expose
    public Integer netAmount;
    @SerializedName("Date")
    @Expose
    public String date;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("addressId")
    @Expose
    public String addressId;
    @SerializedName("userId")
    @Expose
    public UserId userId;

    @SerializedName("comment")
    @Expose
    public String comment="";
}
