package com.codebrew.chillaxprovider.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cbl96 on 16/12/16.
 */
public class PojoPendingOrder {
    public Integer statusCode;
    public String message;
    public List<DatumPendingOrder> data = new ArrayList<>();
}
