package com.codebrew.chillaxprovider.models.pojodirection;

/**
 * Created by cbl89 on 4/10/16.
 */
public class Via_waypoint {
    public End_location location;
    public Integer step_index;
    public Float step_interpolation;
}
