package com.codebrew.chillaxprovider.models;

/*
 * Created by cbl80 on 2/12/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSignUp {

    @SerializedName("data")
    @Expose
    public Data data;

    @SerializedName("otp")
    @Expose
    public Integer otp;
}