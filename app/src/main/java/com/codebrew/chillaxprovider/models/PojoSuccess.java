package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Created by cbl80 on 5/10/16.
 */
public class PojoSuccess {
    @SerializedName("success")
    @Expose
    public Integer success;
    @SerializedName("msg")
    @Expose
    public String msg;

}
