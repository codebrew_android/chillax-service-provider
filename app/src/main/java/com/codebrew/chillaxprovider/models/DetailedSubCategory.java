package com.codebrew.chillaxprovider.models;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl80 on 30/11/16.
 */
public class DetailedSubCategory implements ParentListItem {
    public String subCatName;
    public List<DetSubCaty> category = new ArrayList<>();

    @Override
    public List<?> getChildItemList() {
        return category;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }


}
