package com.codebrew.chillaxprovider.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Created by cbl80 on 5/12/16.
 */
public class MultiDetail implements Parcelable {
    @SerializedName("languageId")
    @Expose
    public String languageId;
    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("name")
    @Expose
    public String name;

    protected MultiDetail(Parcel in) {
        languageId = in.readString();
        _id = in.readString();
        name = in.readString();
    }

    public static final Creator<MultiDetail> CREATOR = new Creator<MultiDetail>() {
        @Override
        public MultiDetail createFromParcel(Parcel in) {
            return new MultiDetail(in);
        }

        @Override
        public MultiDetail[] newArray(int size) {
            return new MultiDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(languageId);
        dest.writeString(_id);
        dest.writeString(name);
    }
}
