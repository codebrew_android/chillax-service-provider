package com.codebrew.chillaxprovider.models.pojodirection;

/**
 * Created by cbl89 on 4/10/16.
 */
public class Step {
    public Distance distance;
    public Distance duration;
    public End_location end_location;
    public String html_instructions;
    public Overview_polyline polyline;
    public End_location start_location;
    public String travel_mode;
    public String maneuver;
}
