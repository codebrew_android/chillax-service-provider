package com.codebrew.chillaxprovider.models.pojodirection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cbl89 on 4/10/16.
 */
public class Leg {
    public Distance distance;
    public Distance duration;
    public String end_address;
    public End_location end_location;
    public String start_address;
    public End_location start_location;
    public List<Step> steps = new ArrayList<Step>();
    public List<Object> traffic_speed_entry = new ArrayList<Object>();
    public List<Via_waypoint> via_waypoint = new ArrayList<Via_waypoint>();
}
