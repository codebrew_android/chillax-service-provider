package com.codebrew.chillaxprovider.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserId implements Parcelable {

    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("lng")
    @Expose
    public double lng=0;
    @SerializedName("lat")
    @Expose
    public double lat=0;
    @SerializedName("address")
    @Expose
    public List<Address> address = new ArrayList<>();
    @SerializedName("profilePicURL")
    @Expose
    public ProfilePicURL profilePicURL;
    @SerializedName("name")
    @Expose
    public String name;

    protected UserId(Parcel in) {
        _id = in.readString();
        lng = in.readDouble();
        lat = in.readDouble();
        address = in.createTypedArrayList(Address.CREATOR);
        profilePicURL = in.readParcelable(ProfilePicURL.class.getClassLoader());
        name = in.readString();
    }

    public static final Creator<UserId> CREATOR = new Creator<UserId>() {
        @Override
        public UserId createFromParcel(Parcel in) {
            return new UserId(in);
        }

        @Override
        public UserId[] newArray(int size) {
            return new UserId[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeDouble(lng);
        dest.writeDouble(lat);
        dest.writeTypedList(address);
        dest.writeParcelable(profilePicURL, flags);
        dest.writeString(name);
    }
}