package com.codebrew.chillaxprovider.models;

import android.os.Parcel;
import android.os.Parcelable;
/*
 * Created by cbl96 on 21/12/16.
 */
public class Address implements Parcelable {
    public String _id;
    public String phoneNo;
    public String date;
    public String phoneNumber;
    public String accessCode;
    public String no;
    public String address;

    protected Address(Parcel in) {
        _id = in.readString();
        phoneNo = in.readString();
        date = in.readString();
        phoneNumber = in.readString();
        accessCode = in.readString();
        no = in.readString();
        address = in.readString();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeString(phoneNo);
        dest.writeString(date);
        dest.writeString(phoneNumber);
        dest.writeString(accessCode);
        dest.writeString(no);
        dest.writeString(address);
    }
}
