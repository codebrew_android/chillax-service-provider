package com.codebrew.chillaxprovider.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SupplierId {

@SerializedName("_id")
@Expose
public String _id;
@SerializedName("deviceToken")
@Expose
public String deviceToken;
@SerializedName("deviceType")
@Expose
public String deviceType;
@SerializedName("password")
@Expose
public String password;
@SerializedName("appVersion")
@Expose
public String appVersion;
@SerializedName("lng")
@Expose
public String lng;
@SerializedName("lat")
@Expose
public String lat;
@SerializedName("address")
@Expose
public List<Object> address = null;
@SerializedName("totalUserRated")
@Expose
public Integer totalUserRated;
@SerializedName("reviews")
@Expose
public Integer reviews;
@SerializedName("ratings")
@Expose
public Integer ratings;
@SerializedName("status")
@Expose
public Boolean status;
@SerializedName("categorySelect")
@Expose
public Boolean categorySelect;
@SerializedName("categories")
@Expose
public List<Category> categories = null;
@SerializedName("product")
@Expose
public List<String> product = null;
@SerializedName("otp")
@Expose
public Integer otp;
@SerializedName("myPoint")
@Expose
public Integer myPoint;
@SerializedName("serachDistance")
@Expose
public Integer serachDistance;
@SerializedName("phoneVerifiy")
@Expose
public Boolean phoneVerifiy;
@SerializedName("notification")
@Expose
public Boolean notification;
@SerializedName("profilePicURL")
@Expose
public ProfilePicURL profilePicURL;
@SerializedName("isBlocked")
@Expose
public Boolean isBlocked;
@SerializedName("registrationDate")
@Expose
public String registrationDate;
@SerializedName("codeUpdatedAt")
@Expose
public String codeUpdatedAt;
@SerializedName("email")
@Expose
public String email;
@SerializedName("phoneNo")
@Expose
public String phoneNo;
@SerializedName("card")
@Expose
public List<Object> card = null;
@SerializedName("countryCode")
@Expose
public String countryCode;
@SerializedName("name")
@Expose
public String name;
@SerializedName("multiDetails")
@Expose
public List<Object> multiDetails = null;
@SerializedName("__v")
@Expose
public Integer __v;
@SerializedName("accessToken")
@Expose
public String accessToken;
@SerializedName("currentBooking")
@Expose
public Boolean currentBooking;

}