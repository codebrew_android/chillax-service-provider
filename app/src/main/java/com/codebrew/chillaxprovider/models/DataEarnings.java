package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl80 on 27/12/16.
 */
public class DataEarnings {
    @SerializedName("monthly_earnings")
    @Expose
    public List<Monthly_earning> monthly_earnings = new ArrayList<>();
    @SerializedName("totalEarnings")
    @Expose
    public String totalEarnings;
}
