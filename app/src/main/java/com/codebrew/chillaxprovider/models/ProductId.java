package com.codebrew.chillaxprovider.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/*
 * Created by cbl96 on 21/12/16.
 */
public class ProductId  implements Parcelable{


    public String _id;
    public List<MultiDetail> multiDetails = null;

    protected ProductId(Parcel in) {
        _id = in.readString();
        multiDetails = in.createTypedArrayList(MultiDetail.CREATOR);
    }

    public static final Creator<ProductId> CREATOR = new Creator<ProductId>() {
        @Override
        public ProductId createFromParcel(Parcel in) {
            return new ProductId(in);
        }

        @Override
        public ProductId[] newArray(int size) {
            return new ProductId[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeTypedList(multiDetails);
    }
}
