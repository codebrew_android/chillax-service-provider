package com.codebrew.chillaxprovider.models.pojodirection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cbl89 on 4/10/16.
 */
public class Geocoded_waypoint {

    public String geocoder_status;
    public String place_id;
    public List<String> types = new ArrayList<String>();
}
