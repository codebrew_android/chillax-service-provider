package com.codebrew.chillaxprovider.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfilePicURL implements Parcelable {

    @SerializedName("thumbnail")
    @Expose
    public String thumbnail;
    @SerializedName("original")
    @Expose
    public String original;

    protected ProfilePicURL(Parcel in) {
        thumbnail = in.readString();
        original = in.readString();
    }

    public static final Creator<ProfilePicURL> CREATOR = new Creator<ProfilePicURL>() {
        @Override
        public ProfilePicURL createFromParcel(Parcel in) {
            return new ProfilePicURL(in);
        }

        @Override
        public ProfilePicURL[] newArray(int size) {
            return new ProfilePicURL[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnail);
        dest.writeString(original);
    }
}