package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl80 on 26/12/16.
 */
public class DatumCity {
    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("multiDetails")
    @Expose
    public List<MultiDetail> multiDetails = new ArrayList<>();
}
