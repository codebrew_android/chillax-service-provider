package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Created by cbl80 on 5/12/16.
 */
public class DataLanguage {
    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("order")
    @Expose
    public Integer order;
    @SerializedName("shortCode")
    @Expose
    public String shortCode;
    @SerializedName("name")
    @Expose
    public String name;
}
