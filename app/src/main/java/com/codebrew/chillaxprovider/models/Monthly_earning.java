package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Monthly_earning {

@SerializedName("month")
@Expose
public Integer month;
@SerializedName("year")
@Expose
public Integer year;
@SerializedName("monthlySalary")
@Expose
public String monthlySalary;

}