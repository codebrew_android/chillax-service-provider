package com.codebrew.chillaxprovider.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by cbl96 on 16/12/16.
 */
public class DatumPendingOrder  implements Parcelable{
    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("supplierReport")
    @Expose
    public Boolean supplierReport;
    @SerializedName("report")
    @Expose
    public Boolean report;
    @SerializedName("details")
    @Expose
    public List<DetailOrder> details = null;
    @SerializedName("netAmount")
    @Expose
    public float netAmount;
    @SerializedName("Date")
    @Expose
    public String date;
    @SerializedName("userId")
    @Expose
    public UserId userId;
    @SerializedName("supplierId")
    @Expose
    public String supplierId;
    @SerializedName("__v")
    @Expose
    public Integer __v;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("reason")
    @Expose
    public String comment;

    protected DatumPendingOrder(Parcel in) {
        _id = in.readString();
        details = in.createTypedArrayList(DetailOrder.CREATOR);
        netAmount = in.readFloat();
        date = in.readString();
        userId = in.readParcelable(UserId.class.getClassLoader());
        supplierId = in.readString();
        status = in.readString();
        comment = in.readString();
    }

    public static final Creator<DatumPendingOrder> CREATOR = new Creator<DatumPendingOrder>() {
        @Override
        public DatumPendingOrder createFromParcel(Parcel in) {
            return new DatumPendingOrder(in);
        }

        @Override
        public DatumPendingOrder[] newArray(int size) {
            return new DatumPendingOrder[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeTypedList(details);
        dest.writeFloat(netAmount);
        dest.writeString(date);
        dest.writeParcelable(userId, flags);
        dest.writeString(supplierId);
        dest.writeString(status);
        dest.writeString(comment);
    }
}
