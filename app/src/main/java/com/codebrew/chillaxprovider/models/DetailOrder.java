package com.codebrew.chillaxprovider.models;

import android.os.Parcel;
import android.os.Parcelable;

/*
 * Created by cbl96 on 16/12/16.
 */
public class DetailOrder implements Parcelable{
    public String _id;
    public ProductId productId;


    protected DetailOrder(Parcel in) {
        _id = in.readString();
        productId = in.readParcelable(ProductId.class.getClassLoader());
    }

    public static final Creator<DetailOrder> CREATOR = new Creator<DetailOrder>() {
        @Override
        public DetailOrder createFromParcel(Parcel in) {
            return new DetailOrder(in);
        }

        @Override
        public DetailOrder[] newArray(int size) {
            return new DetailOrder[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeParcelable(productId, flags);
    }
}
