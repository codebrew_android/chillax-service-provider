package com.codebrew.chillaxprovider.models;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;


public class ListPackagesSupplier implements ParentListItem {
    public String subCatName;
    public List<SubCategories> category = new ArrayList<>();

    @Override
    public List<?> getChildItemList() {
        return category;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }


}