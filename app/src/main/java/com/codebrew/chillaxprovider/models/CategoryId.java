package com.codebrew.chillaxprovider.models;

/*
 * Created by cbl80 on 8/12/16.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryId {

    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("multiDetails")
    @Expose
    public List<MultiDetail> multiDetails = null;

}
