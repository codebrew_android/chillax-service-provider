package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Detail {

    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("quantity")
    @Expose
    public Integer quantity;
    @SerializedName("price")
    @Expose
    public Integer price;
    @SerializedName("productId")
    @Expose
    public ProductId productId;

    @SerializedName("multiDetails")
    @Expose
    public List<MultiDetail> multiDetails = new ArrayList<>();

}