package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("phoneNo")
    @Expose
    public String phoneNo;
    @SerializedName("deviceToken")
    @Expose
    public String deviceToken;
    @SerializedName("deviceType")
    @Expose
    public String deviceType;
    @SerializedName("countryCode")
    @Expose
    public String countryCode;

    @SerializedName("accessToken")
    @Expose
    public String accessToken;
    @SerializedName("otp")
    @Expose
    public Integer otp;
    @SerializedName("serachDistance")
    @Expose
    public Integer serachDistance;
    @SerializedName("phoneVerifiy")
    @Expose
    public Boolean phoneVerifiy;
    @SerializedName("notification")
    @Expose
    public Boolean notification;
    @SerializedName("profilePicURL")
    @Expose
    public ProfilePicURL profilePicURL;

    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("categorySelect")
    @Expose
    public Boolean categorySelect;



}