package com.codebrew.chillaxprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PojoAllLanguage {

@SerializedName("statusCode")
@Expose
public Integer statusCode;
@SerializedName("message")
@Expose
public String message;
@SerializedName("data")
@Expose
public List<DataLanguage> data = new ArrayList<>();

}