package com.codebrew.chillaxprovider.models.pojodirection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cbl89 on 4/10/16.
 */
public class RouteList {
    public Bounds bounds;
    public String copyrights;
    public List<Leg> legs = new ArrayList<Leg>();
    public Overview_polyline overview_polyline;
    public String summary;
    public List<Object> warnings = new ArrayList<Object>();
    public List<Object> waypoint_order = new ArrayList<Object>();
}
