package com.codebrew.chillaxprovider.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.adapters.ServiceAdapter;
import com.codebrew.chillaxprovider.models.Data;
import com.codebrew.chillaxprovider.models.DataProfile;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.utils.DataNames;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/*
 * Created by cbl80 on 22/11/16.
 */
public class FragCategories extends Fragment {

    private static final String ARG_PAGE_NUMBER = "arg_page_number";

    @Bind(R.id.recylerView)
    RecyclerView recylerView;

    public static FragCategories newInstance(int page) {
        FragCategories fragment = new FragCategories();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_recylerview, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DataProfile dataProfile= Prefs.with(getActivity()).getObject(DataNames.USER_PROFILE,DataProfile.class);
        recylerView.setLayoutManager(new GridLayoutManager(getActivity(),3));
        recylerView.setAdapter(new ServiceAdapter(getActivity(),dataProfile.categories));

    }
}
