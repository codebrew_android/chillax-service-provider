package com.codebrew.chillaxprovider.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codebrew.chillaxprovider.R;

import butterknife.ButterKnife;

/*
 * Created by cbl80 on 30/11/16.
 */
public class FragPersonalInfo extends Fragment {

    private static final String ARG_PAGE_NUMBER = "arg_page_number";


    public static FragPersonalInfo newInstance(int page) {
        FragPersonalInfo fragment = new FragPersonalInfo();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }
}
