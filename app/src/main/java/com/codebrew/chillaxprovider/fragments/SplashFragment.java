package com.codebrew.chillaxprovider.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.activities.HomeActivity;
import com.codebrew.chillaxprovider.activities.SplashActivity;
import com.codebrew.chillaxprovider.activities.SubScribeServiceActivity;
import com.codebrew.chillaxprovider.models.PojoSignUp;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.CustomTypefaceSpan;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.dialogs.OtpDialog;
import com.codebrew.chillaxprovider.utils.dialogs.PhoneNumberDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by cbl80 on 21/11/16.
 */
public class SplashFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 101;
    @Bind(R.id.tvFacebook)
    TextView tvFacebook;

    @Bind(R.id.tvGoogle)
    TextView tvGoogle;

    @Bind(R.id.tvSignUp)
    TextView tvSignUp;

    @Bind(R.id.tvLogin)
    TextView tvLogin;

    @Bind(R.id.tvTerms)
    TextView tvTerms;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_splash, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setTypeface();
        initialize();
    }

    private void initialize() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage(getActivity(), this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
    }

    private void setTypeface() {
        tvFacebook.setTypeface(AppGlobal.avn_medium);
        tvGoogle.setTypeface(AppGlobal.avn_medium);
        tvSignUp.setTypeface(AppGlobal.avn_medium);
        tvLogin.setTypeface(AppGlobal.avn_medium);
        SpannableString stTerms = new SpannableString(getString(R.string.terms));
        int index = getString(R.string.terms_and_con).length();
        stTerms.setSpan(new CustomTypefaceSpan("", AppGlobal.avn_book), 0, stTerms.length() - index, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        stTerms.setSpan(new CustomTypefaceSpan("", AppGlobal.avn_heavy), stTerms.length() - index, stTerms.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvTerms.setText(stTerms);
    }

    @OnClick({R.id.tvLogin, R.id.tvSignUp, R.id.tvFacebook, R.id.tvGoogle})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvLogin:
                ((SplashActivity) getActivity()).pushFragments(DataNames.TAB1, new LoginFragment(), true, true, "login", false, false);
                break;
            case R.id.tvSignUp:
                ((SplashActivity) getActivity()).pushFragments(DataNames.TAB1, new SignUpFragment(), true, true, "signup", false, false);
                break;
            case R.id.tvFacebook:
                ((SplashActivity) getActivity()).performLogin();
                break;
            case R.id.tvGoogle:
                signInWithGoogle();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getActivity(), connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    private void signInWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            gmailLogin(acct);
            // mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //updateUI(true);
        }
    }

    private void gmailLogin(GoogleSignInAccount acct) {
        ProgressBarDialog.showProgressBar(getActivity());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("appVersion", "" + GeneralFunctions.getVersion(getActivity()));
        hashMap.put("deviceToken", FirebaseInstanceId.getInstance().getToken());
        hashMap.put("deviceType", "ANDROID");
        hashMap.put("email", acct.getEmail());
        hashMap.put("mailId", acct.getId());
        hashMap.put("name", acct.getDisplayName());
        if (acct.getPhotoUrl() != null) {
            hashMap.put("image", acct.getPhotoUrl().getPath());
        }
        Call<PojoSignUp> gmailCall = RestClient.getModalApiService().loginWithGoogle(hashMap);

        gmailCall.enqueue(new Callback<PojoSignUp>() {
            @Override
            public void onResponse(Call<PojoSignUp> call, Response<PojoSignUp> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    Prefs.with(getActivity()).save(DataNames.USER_DATA, response.body().data.data);
                    if (response.body().data.data.phoneVerifiy) {
                        if (response.body().data.data.categorySelect) {
                            startActivity(new Intent(getActivity(), HomeActivity.class));
                        } else {
                            startActivity(new Intent(getActivity(), SubScribeServiceActivity.class));
                        }
                        getActivity().overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                        getActivity().finishAffinity();
                    } else {
                        phoneDialog(response.body().data.data.accessToken);
                    }
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), getActivity()
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSignUp> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void phoneDialog(final String accessToken) {
        PhoneNumberDialog phoneNumberDilaog = new PhoneNumberDialog(getActivity(), new PhoneNumberDialog.OnOkClickListener() {
            @Override
            public void onButtonClick(String number, String countryCode) {
                phoneNumberUpdate(accessToken, number, countryCode);
            }
        });
        phoneNumberDilaog.show();
    }

    private void phoneNumberUpdate(String accessToken, String number, String countryCode) {
        ProgressBarDialog.showProgressBar(getActivity());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("phoneNo", number);
        hashMap.put("countryCode", countryCode);

        Call<PojoSignUp> numberCall = RestClient.getModalApiService().phoneNumberUpdate("bearer " + accessToken, hashMap);

        numberCall.enqueue(new Callback<PojoSignUp>() {
            @Override
            public void onResponse(Call<PojoSignUp> call, Response<PojoSignUp> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    showOtpDilog(response.body().data.otp);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), getActivity()
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSignUp> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showOtpDilog(Integer otp) {
        OtpDialog otpDialog = new OtpDialog(getActivity(), new OtpDialog.OnOkClickListener() {
            @Override
            public void onButtonClick(String otp) {
                verifyOtp(otp);
            }
        }, otp);
        otpDialog.show();
    }

    private void verifyOtp(String otp) {
        ProgressBarDialog.showProgressBar(getActivity());
        Call<PojoSuccess> otpCall = RestClient.getModalApiService().verifyOtp(StaticFunction.getAccessToken(getActivity())
                , otp);

        otpCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    startActivity(new Intent(getActivity(), SubScribeServiceActivity.class));
                    getActivity().overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                    getActivity().finishAffinity();
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), getActivity(), response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
