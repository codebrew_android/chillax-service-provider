package com.codebrew.chillaxprovider.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.codebrew.chillaxprovider.R;
import com.codebrew.chillaxprovider.activities.HomeActivity;
import com.codebrew.chillaxprovider.activities.SplashActivity;
import com.codebrew.chillaxprovider.activities.SubScribeServiceActivity;
import com.codebrew.chillaxprovider.models.PojoSignUp;
import com.codebrew.chillaxprovider.models.PojoSuccess;
import com.codebrew.chillaxprovider.retrofit.Prefs;
import com.codebrew.chillaxprovider.retrofit.RestClient;
import com.codebrew.chillaxprovider.utils.AppGlobal;
import com.codebrew.chillaxprovider.utils.CustomTypefaceSpan;
import com.codebrew.chillaxprovider.utils.DataNames;
import com.codebrew.chillaxprovider.utils.GeneralFunctions;
import com.codebrew.chillaxprovider.utils.ProgressBarDialog;
import com.codebrew.chillaxprovider.utils.StaticFunction;
import com.codebrew.chillaxprovider.utils.dialogs.CityDialog;
import com.codebrew.chillaxprovider.utils.dialogs.OtpDialog;
import com.codebrew.chillaxprovider.utils.dialogs.PhoneNumberDialog;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by cbl80 on 21/11/16.
 */
public class SignUpFragment extends Fragment {

    @Bind(R.id.tiEmail)
    TextInputLayout tiEmail;

    @Bind(R.id.etEmail)
    EditText etEmail;

    @Bind(R.id.tiPwd)
    TextInputLayout tiPwd;

    @Bind(R.id.tvLogin)
    TextView tvLogin;

    @Bind(R.id.tvTerms)
    TextView tvTerms;

    @Bind(R.id.tiName)
    TextInputLayout tiName;

    @Bind(R.id.etName)
    EditText etName;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Bind(R.id.tvShow)
    CheckBox tvShow;

    @Bind(R.id.etCity)
    TextView etCity;

    private String cityId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_signup, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setTypeface();
        tvShow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int start, end;
                if (!isChecked) {
                    start = etPassword.getSelectionStart();
                    end = etPassword.getSelectionEnd();
                    etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etPassword.setSelection(start, end);
                } else {
                    start = etPassword.getSelectionStart();
                    end = etPassword.getSelectionEnd();
                    etPassword.setTransformationMethod(null);
                    etPassword.setSelection(start, end);
                }
            }
        });
    }

    private void setTypeface() {
        tiEmail.setTypeface(AppGlobal.avn_book);
        etEmail.setTypeface(AppGlobal.avn_book);
        tiPwd.setTypeface(AppGlobal.avn_book);
        tvLogin.setTypeface(AppGlobal.avn_book);
        tiName.setTypeface(AppGlobal.avn_book);
        etName.setTypeface(AppGlobal.avn_book);
        SpannableString stTerms = new SpannableString(getString(R.string.already_member));
        int index = getString(R.string.login).length();
        stTerms.setSpan(new CustomTypefaceSpan("", AppGlobal.avn_book), 0, stTerms.length() - index, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        stTerms.setSpan(new CustomTypefaceSpan("", AppGlobal.avn_heavy), stTerms.length() - index, stTerms.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvTerms.setText(stTerms);
    }

    @OnClick({R.id.tvTerms, R.id.tvLogin, R.id.etCity})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTerms:
                ((SplashActivity) getActivity()).pushFragments(DataNames.TAB1, new LoginFragment(), true, false, "login", false, false);
                break;
            case R.id.tvLogin:
                if (isVerified()) {
                    PhoneNumberDialog phoneNumberDilaog = new PhoneNumberDialog(getActivity(), new PhoneNumberDialog.OnOkClickListener() {
                        @Override
                        public void onButtonClick(String number, String countryCode) {
                            SignupApi(number, countryCode);
                        }
                    });
                    phoneNumberDilaog.show();

                }
                break;
            case R.id.etCity:
                createAlertDialog();
                break;
        }
    }

    private void SignupApi(String number, String countryCode) {
        ProgressBarDialog.showProgressBar(getActivity());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("phoneNo", number);
        hashMap.put("appVersion", "" + GeneralFunctions.getVersion(getActivity()));
        hashMap.put("deviceToken", FirebaseInstanceId.getInstance().getToken());
        hashMap.put("deviceType", "ANDROID");
        hashMap.put("email", etEmail.getText().toString().trim());
        hashMap.put("password", etPassword.getText().toString().trim());
        hashMap.put("countryCode", countryCode);
        hashMap.put("name", etName.getText().toString().trim());
        hashMap.put("cityId",cityId);

        Call<PojoSignUp> signUpCall = RestClient.getModalApiService().signUp(hashMap);
        signUpCall.enqueue(new Callback<PojoSignUp>() {
            @Override
            public void onResponse(Call<PojoSignUp> call, Response<PojoSignUp> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    Prefs.with(getActivity()).save(DataNames.USER_DATA, response.body().data.data);
                    showOtpDilog(response.body().data.data.otp);
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), getActivity()
                                , response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSignUp> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean isVerified() {
        if (etName.getText().toString().trim().length() == 0) {
            etName.requestFocus();
            etName.setError(getString(R.string.error_user_name));
        } else if (etEmail.getText().toString().trim().length() == 0) {
            etEmail.setError(getString(R.string.empty_email));
            etEmail.requestFocus();
        } else if (!GeneralFunctions.isValidEmail(etEmail.getText().toString().trim())) {
            etEmail.setError(getString(R.string.invalid_email));
            etEmail.requestFocus();
        } else if (etCity.getText().toString().trim().length() == 0) {
            etCity.setError(getString(R.string.empty_password));
            etCity.requestFocus();
        } else if (etPassword.getText().toString().trim().length() == 0) {
            etPassword.setError(getString(R.string.empty_password));
            etPassword.requestFocus();
        } else if (etPassword.getText().toString().trim().length() < 6) {
            etPassword.setError(getString(R.string.passwrd_lenght));
            etPassword.requestFocus();
        } else {
            return true;
        }
        return false;
    }

    private void showOtpDilog(Integer otp) {
        OtpDialog otpDialog = new OtpDialog(getActivity(), new OtpDialog.OnOkClickListener() {
            @Override
            public void onButtonClick(String otp) {
                verifyOtp(otp);
            }
        }, otp);
        otpDialog.show();
    }

    private void verifyOtp(String otp) {
        ProgressBarDialog.showProgressBar(getActivity());
        Call<PojoSuccess> otpCall = RestClient.getModalApiService().verifyOtp(StaticFunction.getAccessToken(getActivity())
                , otp);

        otpCall.enqueue(new Callback<PojoSuccess>() {
            @Override
            public void onResponse(Call<PojoSuccess> call, Response<PojoSuccess> response) {
                ProgressBarDialog.dismissProgressDialog();
                if (response.isSuccessful()) {
                    startActivity(new Intent(getActivity(), SubScribeServiceActivity.class));
                    getActivity().overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast);
                    getActivity().finishAffinity();
                } else {
                    try {
                        StaticFunction.handleError(response.errorBody().string(), getActivity(), response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoSuccess> call, Throwable t) {
                ProgressBarDialog.dismissProgressDialog();
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createAlertDialog() {
        CityDialog cityDialog = new CityDialog(getActivity(), new CityDialog.OnOkClickListener() {
            @Override
            public void onButtonClick(String number, String countryCode) {
                etCity.setText(number);
                cityId = countryCode;
            }
        });
        cityDialog.show();
    }
}